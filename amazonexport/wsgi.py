"""
WSGI config for amazonexport project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys

# from django.core.wsgi import get_wsgi_application
# from whitenoise.django import DjangoWhiteNoise

sys.path.append('/root/amazoneexport/amazonexport/')
sys.path.append('/root/virtualenvs/exportproject/lib/python2.7/site-packages')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "amazonexport.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
#application = DjangoWhiteNoise(application)