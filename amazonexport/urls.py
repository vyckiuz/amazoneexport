"""amazonexport URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from products.views import *
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views

admin.autodiscover()
urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^templates/', include('templates_app.urls')),
    url(r'^generate/$', generate, name="generate"),
    url(r'^upload/$', upload, name="upload"),
    url(r'^mass-upload/$', mass_upload, name="mass_upload"),
    url(r'^designs/$', designs, name="designs"),
    url(r'^designs/edit/(?P<design_id>[\w-]+)/$', edit_design, name="edit_design"),
    url(r'^designs/update/(?P<design_id>[\w-]+)/$', update_design, name="update_design"),
    url(r'^admin/', admin.site.urls),
    url(r'^generated-files/$', generated_files, name="generated_files"),
    url(r'^accounts/login/$', views.LoginView.as_view(), name="login"),
    url(r'^failed-designs/(?P<generation_id>[\w-]+)/$', failed_designs, name="failed_designs"),
    url(r'^assign_accounts/$', assign_accounts, name="assign_account"),
    # url(r'^import_accounts/$', import_accounts, name="import_acccounts"),
    url(r'^import_account_designs/$', import_account_designs, name="import_account_designs"),
    url(r'^delete_sku_codes/$', delete_sku_codes, name="delete_sku_codes"),
    url(r'^delete_account_designs/$', delete_account_designs, name="delete_account_designs"),
    url(r'^delete_batch/(?P<batch_id>[\w-]+)/$', delete_batch, name='delete_batch'),
    url(r'^mass-download/$', mass_download, name='mass_download'),

    url(
        regex=r'^logout/$',
        view=views.logout,
        kwargs={'next_page': '/'},
        name='logout'
    ),

]

urlpatterns += static(settings.STATIC_URL,
                      document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT)
