# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-05-07 21:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('templates_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Product',
            new_name='ProductTemplate',
        ),
    ]
