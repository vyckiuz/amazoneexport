from django.contrib import admin
from .models import ProductTemplate, Language
# Register your models here.


admin.site.register(ProductTemplate)
admin.site.register(Language)
# admin.site.register(TiffThumbnails)