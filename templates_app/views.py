# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.forms import ModelForm
from .models import ProductTemplate, Language
from products.models import ProductType
from django import forms
from django.views.decorators.csrf import csrf_exempt
from openpyxl import load_workbook, Workbook
from io import BytesIO
import os



class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()

class ProductTemplateForm(ModelForm):
    class Meta:
        model = ProductTemplate
        exclude = ["language", "product"]

def templates_hub(request):
    # object_fields = ProductTemplate._meta.get_fields()
    # for field in object_fields:
    #     print(field)
    # return HttpResponse(object_fields)

    main_message = "Not imported: \n"
    message = ""
    save = True
    with_parent = False
    
    # product_type = ProductType(name = "Bags")
    # product_type.save()

    if request.method == 'POST' and request.FILES['myfile']:
        if request.POST.get('parent', ""):
            with_parent = True

        file_name = request.FILES['myfile'].name
        file_name = file_name.replace(".xlsx", "").replace(".xls", "")
        file_name_array = file_name.split("_")

        product_type = ProductType.objects.filter(name__iexact = file_name_array[0]).first()
        if not product_type:
            product_type = ProductType(name = file_name_array[0], with_parent = with_parent)
            product_type.save()

        language = Language.objects.filter(short_name__iexact = file_name_array[1]).first()
        if not language:
            language = Language(short_name = file_name_array[1])
            language.save()

        product_template_obj = ProductTemplate.objects.filter(product = product_type).filter(language = language).first()
        if product_template_obj:
            header_dir = check_if_file_exists(product_type, language)
            if header_dir:
                myfile = request.FILES['myfile'].read()
                wb = load_workbook(filename=BytesIO(myfile))
                ws = wb.worksheets[0]
                wb_new = Workbook()
                ws_new = wb_new.active
                row_counter = 1
                while row_counter < 4:
                    column_counter = 1
                    while column_counter < 300:
                        ws_new.cell(row=row_counter, column=column_counter, value=ws.cell(row=row_counter, column=column_counter).value)
                        column_counter = column_counter + 1
                    row_counter = row_counter + 1

                wb_new.save(header_dir)

            return HttpResponse("Jau egzistuoja")

        product_template_obj = ProductTemplate(product = product_type, language = language)

        myfile = request.FILES['myfile'].read()
        wb = load_workbook(filename=BytesIO(myfile))
        ws = wb.worksheets[0]

        indexes = []
        counter = 1
        while counter < 300:
            if ws.cell(row=4, column=counter).value:
                # print("a" + ws.cell(row=4, column=counter).value + "a")
                indexes.append(counter)

            counter = counter + 1

        for counter in indexes:
            cell_value = ws.cell(row=3, column=counter).value
            if hasattr(product_template_obj, cell_value):
                setattr(product_template_obj, cell_value, colnum_string(counter))
            else:
                message = message + cell_value + "\n"
                save = False

        if save == True:
            row_counter = 1
            header_dir = check_if_file_exists(product_type, language)
            if header_dir:
                wb_new = Workbook()
                ws_new = wb_new.active
                while row_counter < 4:
                    column_counter = 1
                    while column_counter < 300:
                        ws_new.cell(row=row_counter, column=column_counter, value=ws.cell(row=row_counter, column=column_counter).value)
                        column_counter = column_counter + 1
                    row_counter = row_counter + 1

                wb_new.save(header_dir)
            product_template_obj.save()
            return HttpResponse("Done")
        else:
            message = main_message + message


        return HttpResponse(message)
        # return HttpResponse(ws.cell(row=1, column=1).value)

        return HttpResponse("Done")

    return render(request, 'templates_app/index.html')

def check_if_file_exists(product_type, language):
    file_header = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/public/headers/"+product_type.name+"/"+language.short_name+".xlsx"

    if not os.path.isfile(file_header):
        data_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/public/headers/"+product_type.name+"/"
        if not os.path.isdir(data_directory):
            os.makedirs(data_directory)
        return file_header

    return ""


def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string



