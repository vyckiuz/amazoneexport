# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from products.models import ProductType


# Create your models here.
class Language(models.Model):
    short_name = models.CharField(blank = False, max_length = 10)
    long_name = models.CharField(blank = True, max_length = 10)

    def __unicode__(self):
        return "%s" % self.short_name

class ProductTemplate(models.Model):
    product = models.ForeignKey(ProductType, on_delete=models.CASCADE, default = 0)
    language = models.ForeignKey(Language, on_delete=models.CASCADE, default = 0)

    item_sku = models.CharField(max_length=5, blank=True)
    item_name = models.CharField(max_length=5, blank=True)
    brand_name = models.CharField(max_length=5, blank=True)
    product_subtype = models.CharField(max_length=5, blank=True)
    product_description = models.CharField(max_length=5, blank=True)
    part_number = models.CharField(max_length=5, blank=True)
    standard_price = models.CharField(max_length=5, blank=True)
    quantity = models.CharField(max_length=5, blank=True)
    recommended_browse_nodes = models.CharField(max_length=5, blank=True)
    generic_keywords = models.CharField(max_length=5, blank=True)
    bullet_point1 = models.CharField(max_length=5, blank=True)
    bullet_point2 = models.CharField(max_length=5, blank=True)
    bullet_point3 = models.CharField(max_length=5, blank=True)
    bullet_point4 = models.CharField(max_length=5, blank=True)
    bullet_point5 = models.CharField(max_length=5, blank=True)
    main_image_url = models.CharField(max_length=5, blank=True)
    other_image_url1 = models.CharField(max_length=5, blank=True)
    other_image_url2 = models.CharField(max_length=5, blank=True)
    parent_child = models.CharField(max_length=5, blank=True)
    parent_sku = models.CharField(max_length=5, blank=True)
    relationship_type = models.CharField(max_length=5, blank=True)
    variation_theme = models.CharField(max_length=5, blank=True)
    country_of_origin = models.CharField(max_length=5, blank=True)
    color_map = models.CharField(max_length=5, blank=True)
    color_name = models.CharField(max_length=5, blank=True)
    size_map = models.CharField(max_length=5, blank=True)
    size_name = models.CharField(max_length=5, blank=True)
    material_composition = models.CharField(max_length=5, blank=True)
    outer_material_type = models.CharField(max_length=5, blank=True)
    department_name = models.CharField(max_length=5, blank=True)
    is_adult_product = models.CharField(max_length=5, blank=True)
    feed_product_type = models.CharField(max_length=5, blank=True)
    manufacturer = models.CharField(max_length=5, blank=True)
    currency = models.CharField(max_length=5, blank=True)
    recommended_browse_nodes1 = models.CharField(max_length=5, blank=True)
    generic_keywords1 = models.CharField(max_length=5, blank=True)
    generic_keywords2 = models.CharField(max_length=5, blank=True)
    generic_keywords3 = models.CharField(max_length=5, blank=True)
    generic_keywords4 = models.CharField(max_length=5, blank=True)
    generic_keywords5 = models.CharField(max_length=5, blank=True)
    material_type = models.CharField(max_length=5, blank=True)
    inner_material_type = models.CharField(max_length=5, blank=True)
    swatch_image_url = models.CharField(max_length=5, blank=True)
    color_map1 = models.CharField(max_length=5, blank=True)
    color_map2 = models.CharField(max_length=5, blank=True)
    material_type1 = models.CharField(max_length=5, blank=True)

    def __unicode__(self):
        return "%s - %s" % (self.product.name, self.language.short_name)

