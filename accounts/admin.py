from django.contrib import admin
from .models import Team, Account, TeamUser
# Register your models here.

admin.site.register(Team)
admin.site.register(Account)
admin.site.register(TeamUser)