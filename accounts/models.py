from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from products.models import Design, AccountDesign

# Create your models here.
class Team(models.Model):
    name = models.CharField(blank = True, max_length = 255)
    def __str__(self):
        return "%s" % self.name

class Account(models.Model):
    name = models.CharField(blank = True, max_length = 255)
    brand_name = models.CharField(blank = True, max_length = 255)
    sku_prefix = models.CharField(blank = True, max_length = 255)
    team = models.ForeignKey('Team', on_delete=models.CASCADE, default = 0)
    # user = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)

    def designs_count(self):
        designs_ids = []
        account_designs = AccountDesign.objects.filter(account = self)
        for account_design in account_designs:
            designs_ids.append(account_design.design.id)

        designs_list = Design.objects.filter(account = self) | Design.objects.filter(id__in = designs_ids)

        return len(designs_list)

    def __str__(self):
        return "Account name: %s, Team: %s" % (self.name, self.team)


class TeamUser(models.Model):
    team = models.ForeignKey('Team', on_delete=models.CASCADE, default = 0)
    user = models.OneToOneField(User, on_delete=models.CASCADE, default = 1)

    def __str__(self):
        return "Team: %s, User: %s" % (self.team, self.user)
