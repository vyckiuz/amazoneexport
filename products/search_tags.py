from urllib.request import urlopen
import time
import re

from bs4 import BeautifulSoup

MAX_LENGTH = 50


class PageNotFoundException(Exception):
    pass


def good_length(keywords):
    if len(", ".join(keywords)) < 150:
        return True
    return False


def good_keyword(keywords, keyword):
    if keyword in keywords:
        return False

    # print(keywords)
    # print(keyword)

    for k in keywords:
        for word in keyword.split(" "):
            if word in k:
                return False

    # for k in keywords:
    #     if k in keyword or keyword in k:
    #         return False

    return True


def get_search_page(query):
    domain = "http://www.redbubble.com/shop/"

    while True:
        try:
            response = urlopen(domain + "+".join(query.split()))
        except urllib2.HTTPError:
            if " " not in query:
                raise PageNotFoundException

            query = query.rsplit(' ', 1)[0]
            continue
        break

    return response


def get_keywords(query):
    response = get_search_page(query)

    page_source = response.read()
    parsed_html = BeautifulSoup(page_source, "lxml")
    regex = re.compile("^styles__normalLink")
    links_list = parsed_html.find_all("a", regex)

    keywords = []

    count = 0
    for link in links_list:
        if "/people/" in link["href"]:
            count += 1

            # print "https://www.redbubble.com" + link["href"]
            response = urlopen("https://www.redbubble.com" + link["href"])
            page_source = response.read()
            # print page_source
            parsed_html = BeautifulSoup(page_source, "lxml")
            # print parsed_html

            # link_keywords = parsed_html.find("meta", attrs={"name": "keywords"})["content"]
            link_keywords = ""

            # print page_source

            # print parsed_html
            tags = parsed_html.find("meta", attrs={"name": "keywords"})["content"].split(", ")

            for new_keyword in tags:
                if new_keyword not in keywords and good_keyword(keywords, new_keyword):
                    keywords.append(new_keyword)

            # raw_input("?")
            #
            # # regex = re.compile("^Tags__tag")
            # # for item in parsed_html.find_all("a", regex):
            # for item in parsed_html.select("a[class*=Tags__tag]"):
            #     print item
            #     # link_keywords += " " + item.contents[0]
            #     print item.contents[0]
            #     if item.contents[0] not in keywords:
            #         new_keyword = item.contents[0]
            #
            #         if good_keyword(keywords, new_keyword):
            #             keywords.append(new_keyword)

            # for k in link_keywords.split(" "):
            #     if good_keyword(keywords, k):
            #         keywords.append(k)
            #         print keywords
            #
            #         if not good_length(keywords):
            #             return keywords

            if len(", ".join(keywords)) >= MAX_LENGTH:
                return keywords

    # print count

    return keywords


def get_search_tags(query):
    try:
        keywords = get_keywords(query)
        s = ""
        return_keywords = []

        for keyword in keywords:
            s += keyword + ","

            if len(s) < MAX_LENGTH:
                return_keywords.append(keyword)
            else:
                return return_keywords

    except PageNotFoundException:
        return ["", "", ""]


if __name__ == "__main__":
    # print "Hi"
    print(get_search_tags("bitcoin"))
