from .utils import concat_search_tags

parent_sku = ""

def get_fields_indexes(ws):
    max_column = ws.max_column
    row_index = 3
    fields_indexes = {}
    counter = 0
    while counter < max_column-1:
        field_value = ws[row_index][counter].value
        if field_value in fields_indexes:
            print("already exist")
        else:
            fields_indexes[field_value] = counter + 1

        counter += 1

    return fields_indexes

def get_UK_fields_values(product, design, color_variation):
    search_tag = concat_search_tags(product.static_search_tags_1, design.get_search_tags(), False)
    fields_values = {}
    fields_values["product_title"] = product.title
    fields_values["product_description"] = product.description
    fields_values["product_type"] = product.type_uk
    fields_values["department_name"] = product.departments_name
    fields_values["material_composition"] = product.material_composition
    fields_values["fabric_type"] = product.fabric_type_uk
    fields_values["bullet_point_1"] = product.bullet_point_1
    fields_values["bullet_point_2"] = product.bullet_point_2
    fields_values["bullet_point_3"] = product.bullet_point_3
    fields_values["bullet_point_4"] = product.bullet_point_4
    fields_values["bullet_point_5"] = product.bullet_point_5
    fields_values["search_tags"] = search_tag
    fields_values["seasons"] = "Spring-Summer"
    fields_values["country_of_origin"] = "Lithuania"
    fields_values["target_gender"] = "Male"
    fields_values["age_range_description"] = "Adult"
    fields_values["shirt_size_system"] = "UK"
    fields_values["shirt_size_class"] = "Alpha"
    fields_values["care_instructions"] = "Machine Wash"
    fields_values["collar_style"] = "Crew neck"
    fields_values["is_autographed"] = "No"
    fields_values["recommended_browse_nodes"] = product.browse_node_uk
    fields_values["color_map"] = color_variation.color_name_en
    fields_values["color_name"] = color_variation.color_name_en
    fields_values["standard_price"] = color_variation.price_gbp
    fields_values["sleeve_type"] = product.sleeve_type

    return fields_values

def get_DE_fields_values(product, design, color_variation):
    search_tag = concat_search_tags(product.static_search_tag_de, design.get_search_tags(), False)
    fields_values = {}
    if product.title_de:
        fields_values["product_title"] = product.title_de
    else:
        fields_values["product_title"] = product.title

    if product.description_de:
        fields_values["product_description"] = product.description_de
    else:
        fields_values["product_description"] = product.description

    if product.type_de:
        fields_values["product_type"] = product.type_de
    else:
        fields_values["product_type"] = product.type_uk

    if product.departments_name_de:
        fields_values["department_name"] = product.departments_name_de
    else:
        fields_values["department_name"] = product.departments_name

    if product.material_composition_de:
        fields_values["material_composition"] = product.material_composition_de
    else:
        fields_values["material_composition"] = product.material_composition

    if product.fabric_type_de:
        fields_values["fabric_type"] = product.fabric_type_de
    else:
        fields_values["fabric_type"] = product.fabric_type_uk

    if product.bullet_point_1_de:
        fields_values["bullet_point_1"] = product.bullet_point_1_de
    else:
        fields_values["bullet_point_1"] = product.bullet_point_1

    if product.bullet_point_2_de:
        fields_values["bullet_point_2"] = product.bullet_point_2_de
    else:
        fields_values["bullet_point_2"] = product.bullet_point_2

    if product.bullet_point_3_de:
        fields_values["bullet_point_3"] = product.bullet_point_3_de
    else:
        fields_values["bullet_point_3"] = product.bullet_point_3

    if product.bullet_point_4_de:
        fields_values["bullet_point_4"] = product.bullet_point_4_de
    else:
        fields_values["bullet_point_4"] = product.bullet_point_4

    if product.bullet_point_5_de:
        fields_values["bullet_point_5"] = product.bullet_point_5_de
    else:
        fields_values["bullet_point_5"] = product.bullet_point_5

    fields_values["search_tags"] = search_tag
    fields_values["seasons"] = "Frühling-Sommer"
    fields_values["country_of_origin"] = "Litauen"
    fields_values["target_gender"] = "Männlich"
    fields_values["age_range_description"] = "Erwachsener"
    fields_values["shirt_size_system"] = "DE / NL / SE / PL"
    fields_values["shirt_size_class"] = "Alphanumerisch"
    fields_values["care_instructions"] = "Maschinenwäsche"
    fields_values["collar_style"] = "Rundkragen"
    fields_values["is_autographed"] = "Nein"
    fields_values["recommended_browse_nodes"] = product.browse_node_de
    fields_values["color_map"] = color_variation.color_name_de
    fields_values["color_name"] = color_variation.color_name_de
    fields_values["standard_price"] = str(color_variation.price_eur).replace(".", ",")
    fields_values["sleeve_type"] = product.sleeve_type_de

    return fields_values

def get_FR_fields_values(product, design, color_variation):
    search_tag = concat_search_tags(product.static_search_tag_fr, design.get_search_tags(), False)
    fields_values = {}
    if product.title_fr:
        fields_values["product_title"] = product.title_fr
    else:
        fields_values["product_title"] = product.title

    if product.description_fr:
        fields_values["product_description"] = product.description_fr
    else:
        fields_values["product_description"] = product.description

    if product.type_fr:
        fields_values["product_type"] = product.type_fr
    else:
        fields_values["product_type"] = product.type_uk

    if product.departments_name_fr:
        fields_values["department_name"] = product.departments_name_fr
    else:
        fields_values["department_name"] = product.departments_name

    if product.material_composition_fr:
        fields_values["material_composition"] = product.material_composition_fr
    else:
        fields_values["material_composition"] = product.material_composition

    if product.fabric_type_fr:
        fields_values["fabric_type"] = product.fabric_type_fr
    else:
        fields_values["fabric_type"] = product.fabric_type_uk

    if product.bullet_point_1_fr:
        fields_values["bullet_point_1"] = product.bullet_point_1_fr
    else:
        fields_values["bullet_point_1"] = product.bullet_point_1

    if product.bullet_point_2_fr:
        fields_values["bullet_point_2"] = product.bullet_point_2_fr
    else:
        fields_values["bullet_point_2"] = product.bullet_point_2

    if product.bullet_point_3_fr:
        fields_values["bullet_point_3"] = product.bullet_point_3_fr
    else:
        fields_values["bullet_point_3"] = product.bullet_point_3

    if product.bullet_point_4_fr:
        fields_values["bullet_point_4"] = product.bullet_point_4_fr
    else:
        fields_values["bullet_point_4"] = product.bullet_point_4

    if product.bullet_point_5_fr:
        fields_values["bullet_point_5"] = product.bullet_point_5_fr
    else:
        fields_values["bullet_point_5"] = product.bullet_point_5

    fields_values["search_tags"] = search_tag
    fields_values["seasons"] = "printemps-été"
    fields_values["country_of_origin"] = "Lituanie"
    fields_values["target_gender"] = "Masculin"
    fields_values["age_range_description"] = "Adulte"
    fields_values["shirt_size_system"] = "FR / ES"
    fields_values["shirt_size_class"] = "Alpha/lettres"
    fields_values["care_instructions"] = "Lavage en machine"
    fields_values["collar_style"] = "Col rond"
    fields_values["is_autographed"] = "Non"
    fields_values["recommended_browse_nodes"] = product.browse_node_fr
    fields_values["color_map"] = color_variation.color_name_fr
    fields_values["color_name"] = color_variation.color_name_fr
    fields_values["standard_price"] = color_variation.price_eur
    fields_values["sleeve_type"] = product.sleeve_type_fr

    return fields_values

def get_ES_fields_values(product, design, color_variation):
    search_tag = concat_search_tags(product.static_search_tag_es, design.get_search_tags(), False)
    fields_values = {}
    if product.title_es:
        fields_values["product_title"] = product.title_es
    else:
        fields_values["product_title"] = product.title

    if product.description_es:
        fields_values["product_description"] = product.description_es
    else:
        fields_values["product_description"] = product.description

    if product.type_es:
        fields_values["product_type"] = product.type_es
    else:
        fields_values["product_type"] = product.type_uk

    if product.departments_name_es:
        fields_values["department_name"] = product.departments_name_es
    else:
        fields_values["department_name"] = product.departments_name

    if product.material_composition_es:
        fields_values["material_composition"] = product.material_composition_es
    else:
        fields_values["material_composition"] = product.material_composition

    if product.fabric_type_es:
        fields_values["fabric_type"] = product.fabric_type_es
    else:
        fields_values["fabric_type"] = product.fabric_type_uk

    if product.bullet_point_1_es:
        fields_values["bullet_point_1"] = product.bullet_point_1_es
    else:
        fields_values["bullet_point_1"] = product.bullet_point_1

    if product.bullet_point_2_es:
        fields_values["bullet_point_2"] = product.bullet_point_2_es
    else:
        fields_values["bullet_point_2"] = product.bullet_point_2

    if product.bullet_point_3_es:
        fields_values["bullet_point_3"] = product.bullet_point_3_es
    else:
        fields_values["bullet_point_3"] = product.bullet_point_3

    if product.bullet_point_4_es:
        fields_values["bullet_point_4"] = product.bullet_point_4_es
    else:
        fields_values["bullet_point_4"] = product.bullet_point_4

    if product.bullet_point_5_es:
        fields_values["bullet_point_5"] = product.bullet_point_5_es
    else:
        fields_values["bullet_point_5"] = product.bullet_point_5

    fields_values["search_tags"] = search_tag
    fields_values["seasons"] = "Spring-Summer"
    fields_values["country_of_origin"] = "Lituania"
    fields_values["target_gender"] = "Masculino"
    fields_values["age_range_description"] = "Adulto"
    fields_values["shirt_size_system"] = "FR / ES"
    fields_values["shirt_size_class"] = "Letras"
    fields_values["care_instructions"] = "Lavar a máquina"
    fields_values["collar_style"] = "clásico"
    fields_values["is_autographed"] = "No"
    fields_values["recommended_browse_nodes"] = product.browse_node_es
    fields_values["color_map"] = color_variation.color_name_es
    fields_values["color_name"] = color_variation.color_name_es
    fields_values["standard_price"] = color_variation.price_eur
    fields_values["sleeve_type"] = product.sleeve_type_es

    return fields_values

def get_IT_fields_values(product, design, color_variation):
    search_tag = concat_search_tags(product.static_search_tag_it, design.get_search_tags(), False)
    fields_values = {}
    if product.title_it:
        fields_values["product_title"] = product.title_it
    else:
        fields_values["product_title"] = product.title

    if product.description_it:
        fields_values["product_description"] = product.description_it
    else:
        fields_values["product_description"] = product.description

    if product.type_it:
        fields_values["product_type"] = product.type_it
    else:
        fields_values["product_type"] = product.type_uk

    if product.departments_name_it:
        fields_values["department_name"] = product.departments_name_it
    else:
        fields_values["department_name"] = product.departments_name

    if product.fabric_type_it:
        fields_values["fabric_type"] = product.fabric_type_it
    else:
        fields_values["fabric_type"] = product.fabric_type_uk

    if product.material_composition_it:
        fields_values["material_composition"] = product.material_composition_it
    else:
        fields_values["material_composition"] = product.material_composition

    if product.bullet_point_1_it:
        fields_values["bullet_point_1"] = product.bullet_point_1_it
    else:
        fields_values["bullet_point_1"] = product.bullet_point_1

    if product.bullet_point_2_it:
        fields_values["bullet_point_2"] = product.bullet_point_2_it
    else:
        fields_values["bullet_point_2"] = product.bullet_point_2

    if product.bullet_point_3_it:
        fields_values["bullet_point_3"] = product.bullet_point_3_it
    else:
        fields_values["bullet_point_3"] = product.bullet_point_3

    if product.bullet_point_4_it:
        fields_values["bullet_point_4"] = product.bullet_point_4_it
    else:
        fields_values["bullet_point_4"] = product.bullet_point_4

    if product.bullet_point_5_it:
        fields_values["bullet_point_5"] = product.bullet_point_5_it
    else:
        fields_values["bullet_point_5"] = product.bullet_point_5

    fields_values["search_tags"] = search_tag
    fields_values["seasons"] = "primavera/estate"
    fields_values["country_of_origin"] = "Lituania"
    fields_values["target_gender"] = "Maschio"
    fields_values["age_range_description"] = "Adulto"
    fields_values["shirt_size_system"] = "IT"
    fields_values["shirt_size_class"] = "Testo"
    fields_values["care_instructions"] = "Lavare in lavatrice"
    fields_values["collar_style"] = "classico"
    fields_values["is_autographed"] = "No"
    fields_values["recommended_browse_nodes"] = product.browse_node_it
    fields_values["color_map"] = color_variation.color_name_it
    fields_values["color_name"] = color_variation.color_name_it
    fields_values["standard_price"] = color_variation.price_eur
    fields_values["sleeve_type"] = product.sleeve_type_it

    return fields_values

def mugs_config(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url, country):
    if country == "UK":
        fields_values = get_UK_fields_values(product, design, color_variation)
    elif country == "DE":
        fields_values = get_DE_fields_values(product, design, color_variation)
    elif country == "FR":
        fields_values = get_FR_fields_values(product, design, color_variation)
    elif country == "ES":
        fields_values = get_ES_fields_values(product, design, color_variation)
    elif country == "IT":
        fields_values = get_IT_fields_values(product, design, color_variation)

    sku = account.sku_prefix + '-' + product.type_short + '-' + color_variation.color_name + '-' + sku_code

    extra_photo_url = ""
    extra_photo_2_url = ""
    extra_photo_3_url = ""
    extra_photo_4_url = ""
    extra_photo_5_url = ""

    if product.extra_photo:
        extra_photo_url = 'http://' + host_name + product.extra_photo.url
    if product.extra_photo_2:
        extra_photo_2_url = 'http://' + host_name + product.extra_photo_2.url
    if product.extra_photo_3:
        extra_photo_3_url = 'http://' + host_name + product.extra_photo_3.url
    if product.extra_photo_4:
        extra_photo_4_url = 'http://' + host_name + product.extra_photo_4.url
    if product.extra_photo_5:
        extra_photo_5_url = 'http://' + host_name + product.extra_photo_5.url


    required_fields = [
        {
            "column_name": "feed_product_type",
            "value": fields_values["product_type"]
        },
        {
            "column_name": "item_sku",
            "value": sku
        },
        {
            "column_name": "brand_name",
            "value": account.brand_name
        },
        {
            "column_name": "external_product_id",
            "value": ""
        },
        {
            "column_name": "external_product_id_type",
            "value": ""
        },
        {
            "column_name": "item_name",
            "value": design.title + ' ' + fields_values["product_title"]
        },
        {
            "column_name": "recommended_browse_nodes",
            "value": fields_values["recommended_browse_nodes"]
        },
        {
            "column_name": "unit_count_type",
            "value": product.capacity_unit_of_measure
        },
        {
            "column_name": "unit_count",
            "value": product.capacity
        },

        {
            "column_name": "material_type",
            "value": fields_values["material_composition"]
        },
        {
            "column_name": "color_map",
            "value": fields_values["color_map"]
        },
        {
            "column_name": "color_name",
            "value": fields_values["color_name"]
        },
        {
            "column_name": "size_name",
            "value": product.size_name
        },
        {
            "column_name": "country_of_origin",
            "value": fields_values["country_of_origin"],
        },
        {
            "column_name": "standard_price",
            "value": fields_values["standard_price"]
        },
        {
            "column_name": "quantity",
            "value": 10
        },
        {
            "column_name": "number_of_items",
            "value": 1
        },
        {
            "column_name": "main_image_url",
            "value": 'http://' + host_name + generated_img_url
        },
        {
            "column_name": "product_description",
            "value": fields_values["product_description"]
        },
        {
            "column_name": "model_name",
            "value": sku
        },
        {
            "column_name": "model",
            "value": sku
        },
        {
            "column_name": "part_number",
            "value": sku
        },
        {
            "column_name": "manufacturer",
            "value": account.brand_name
        },
        {
            "column_name": "generic_keywords",
            "value": fields_values["search_tags"]
        },
        {
            "column_name": "style_name",
            "value": product.style_name
        },
        {
            "column_name": "bullet_point1",
            "value": fields_values["bullet_point_1"]
        },
        {
            "column_name": "bullet_point2",
            "value": fields_values["bullet_point_2"]
        },
        {
            "column_name": "bullet_point3",
            "value": fields_values["bullet_point_3"]
        },
        {
            "column_name": "bullet_point4",
            "value": fields_values["bullet_point_4"]
        },
        {
            "column_name": "bullet_point5",
            "value": fields_values["bullet_point_5"]
        },
        {
            "column_name": "other_image_url1",
            "value": extra_photo_url
        },
        {
            "column_name": "other_image_url2",
            "value": extra_photo_2_url
        },
        {
            "column_name": "other_image_url3",
            "value": extra_photo_3_url
        },
        {
            "column_name": "other_image_url4",
            "value": extra_photo_4_url
        },
        {
            "column_name": "other_image_url5",
            "value": extra_photo_5_url
        },
        {
            "column_name": "item_width_unit_of_measure",
            "value": product.item_width_unit_of_measure
        },
        {
            "column_name": "item_width",
            "value": product.item_width
        },
        {
            "column_name": "item_height",
            "value": product.item_height
        },
        {
            "column_name": "item_height_unit_of_measure",
            "value": product.item_height_unit_of_measure
        },
        {
            "column_name": "item_length_unit_of_measure",
            "value": product.item_length_unit_of_measure,
        },
        {
            "column_name": "item_length",
            "value": product.item_length,
        },
        {
            "column_name": "size_map",
            "value": "One Size",
        },
        {
            "column_name": "liquid_volume",
            "value": product.capacity,
        },
        {
            "column_name": "liquid_volume_unit_of_measure",
            "value": product.capacity_unit_of_measure,
        },
        {
            "column_name": "capacity_unit_of_measure",
            "value": product.capacity_unit_of_measure
        },
        {
            "column_name": "capacity",
            "value": product.capacity
        }
    ]



    return required_fields

def fill_mugs_sheet(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url, country_code):
    fields_indexes = get_fields_indexes(ws)
    row_count = ws.max_row + 1

    config_obj = mugs_config(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url, country_code)

    for config in config_obj:
        column_index = fields_indexes[config["column_name"]]
        ws.cell(row=row_count, column=column_index, value=config["value"])

    row_count += 1

    return products_list

   
