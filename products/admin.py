from django.contrib import admin
from .models import Product, ColorVariation, Size, Design, GeneratedDesigns, GeneratedFile, Generation, SkuCodes, GenerationError, TiffThumbnails, ProductType, Country, Product_New
# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    filter_horizontal = ('sizes',)


class DesignAdmin(admin.ModelAdmin):
    search_fields = ('title', 'sku_code',)


admin.site.register(Product, ProductAdmin)
admin.site.register(ColorVariation)
admin.site.register(Size)
admin.site.register(Design, DesignAdmin)
admin.site.register(GeneratedDesigns)
admin.site.register(GeneratedFile)
admin.site.register(Generation)
# admin.site.register(GenerationError)
# admin.site.register(SkuCodes)
admin.site.register(ProductType)
admin.site.register(Country)
admin.site.register(Product_New, ProductAdmin)
# admin.site.register(TiffThumbnails)