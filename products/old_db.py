import psycopg2
import pprint

class OldDB:
    def __init__(self):
        #Define our connection string
        conn_string = "host='localhost' dbname='casta123' user='postgres' password='asdf1234'"

        # get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(conn_string)
        conn.set_client_encoding('UTF8')
 
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        self.cursor = conn.cursor()

    def get_old_accounts(self):
        self.cursor.execute("SELECT * FROM accounts_account")
 
        # retrieve the records from the database
        records = self.cursor.fetchall()

        return records

    def get_account_designs(self, account_id):
        self.cursor.execute("SELECT * FROM designs_design WHERE account_id = " + str(account_id))
        records = self.cursor.fetchall()
        return records