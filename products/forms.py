from django import forms
from accounts.models import Account


class DesignDownloadForm(forms.Form):
    sku = forms.CharField(max_length=10, widget=forms.TextInput(attrs={"class": "form-control"}))


class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50, widget=forms.TextInput(attrs={"class": "form-control"}), required=False)
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True, "class": "form-control"}))
    search_query = forms.CharField(max_length=50, widget=forms.TextInput(attrs={"class": "form-control"}), required=False)
    accounts1 = forms.ModelChoiceField(queryset=Account.objects.all(), widget=forms.Select(attrs={"class": "form-control"}))
    search_tags = forms.CharField(max_length=50, widget=forms.Textarea(attrs={"class": "form-control", "rows": "3"}), required=False)


class MassUploadForm(forms.Form):
    account = forms.ModelChoiceField(queryset=Account.objects.all(), widget=forms.Select(attrs={"class": "form-control"}))
    file = forms.FileField()


class MassDownloadForm(forms.Form):
    skus = forms.CharField(widget=forms.Textarea(attrs={"class": "form-control"}))
