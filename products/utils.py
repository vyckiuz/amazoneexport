def slice_string(str, length):
    new_string = ""

    for item in str.split(" "):
        if len(new_string + " " + item) <= length:
            new_string += " " + item

    return new_string


def concat_search_tags(static_tag, design_tag, split=False):
    new_string = " ".join([static_tag, design_tag])

    if not split:
        return new_string

    l = ["", "", "", "", ""]

    l_index = 0
    for item in new_string.split(" "):
        l_item = l[l_index]
        if len(l_item + " " + item) <= 50:
            l[l_index] += " " + item
        else:
            l_index += 1

            if l_index > 4:
                break

    return l


if __name__ == "__main__":
    print(concat_search_tags("Maikon IT tag Maikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaikon IT tagMaik", "labas labas labas labas labas labas labas labas la", True))