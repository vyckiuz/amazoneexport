from .utils import concat_search_tags

parent_sku = ""


def fill_UK_bags_sheet(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url):
    row_count = ws.max_row + 1
    sku = account.sku_prefix + '-' + product.type_short + '-' + color_variation.color_name + '-' + sku_code

    global parent_sku
    parent_sku = sku
    column_count = 1
    ws.cell(row=row_count, column=column_count, value=product.type_uk)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=account.brand_name)

    column_count = column_count + 3
    ws.cell(row=row_count, column=column_count, value=design.title + " " + product.title)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.browse_node_uk)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.material_composition)

    column_count = column_count + 2
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_en)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_en)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.price_gbp)

    # column_count = column_count + 1
    # ws.cell(row=row_count, column=column_count, value="GBP")

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=10)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value='http://' + host_name + generated_img_url)

    column_count = column_count + 1
    if product.extra_photo:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo.url)

    column_count = column_count + 1
    if product.extra_photo_2:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo_2.url)

    column_count = column_count + 13
    ws.cell(row=row_count, column=column_count, value=product.description)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 9
    ws.cell(row=row_count, column=column_count, value=product.bullet_point_1)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.bullet_point_2)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.bullet_point_3)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.bullet_point_4)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.bullet_point_5)

    search_tags = concat_search_tags(product.static_search_tags_1, design.get_search_tags(), False)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=search_tags)

    column_count = column_count + 6
    ws.cell(row=row_count, column=column_count, value="One size")

    # column_count = column_count + 27
    # ws.cell(row=row_count, column=column_count, value="One size")

    row_count = row_count + 1

    return products_list


def fill_DE_bags_sheet(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url):
    if product.static_search_tag_de:
        search_tag = product.static_search_tag_de
    else:
        search_tag = product.static_search_tag

    if product.title_de:
        product_title = product.title_de
    else:
        product_title = product.title

    if product.description_de:
        product_description = product.description_de
    else:
        product_description = product.description

    if product.type_de:
        product_type = product.type_de
    else:
        product_type = product.type_uk

    if product.departments_name_de:
        departments_name = product.departments_name_de
    else:
        departments_name = product.departments_name

    if product.material_composition_de:
        material_composition = product.material_composition_de
    else:
        material_composition = product.material_composition

    if product.bullet_point_1_de:
        bullet_point_1 = product.bullet_point_1_de
    else:
        bullet_point_1 = product.bullet_point_1

    if product.bullet_point_2_de:
        bullet_point_2 = product.bullet_point_2_de
    else:
        bullet_point_2 = product.bullet_point_2

    if product.bullet_point_3_de:
        bullet_point_3 = product.bullet_point_3_de
    else:
        bullet_point_3 = product.bullet_point_3

    if product.bullet_point_4_de:
        bullet_point_4 = product.bullet_point_4_de
    else:
        bullet_point_4 = product.bullet_point_4

    if product.bullet_point_5_de:
        bullet_point_5 = product.bullet_point_5_de
    else:
        bullet_point_5 = product.bullet_point_5

    row_count = ws.max_row + 1
    sku = account.sku_prefix + '-' + product.type_short + '-' + color_variation.color_name + '-' + sku_code

    column_count = 1
    global parent_sku
    parent_sku = sku
    ws.cell(row=row_count, column=column_count, value=product.type_de)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=account.brand_name)

    column_count = column_count + 3
    ws.cell(row=row_count, column=column_count, value=design.title + " " + product.title_de)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.browse_node_de)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=material_composition)

    column_count = column_count + 2
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_de)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_de)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.price_eur)

    # column_count = column_count + 1
    # ws.cell(row=row_count, column=column_count, value="GBP")

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=10)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value='http://' + host_name + generated_img_url)

    column_count = column_count + 1
    if product.extra_photo:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo.url)

    column_count = column_count + 1
    if product.extra_photo_2:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo_2.url)

    column_count = column_count + 13
    ws.cell(row=row_count, column=column_count, value=product_description)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 8
    ws.cell(row=row_count, column=column_count, value=bullet_point_1)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_2)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_3)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_4)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_5)

    search_tags = concat_search_tags(product.static_search_tag_de, design.get_search_tags(), False)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=search_tags)

    column_count = column_count + 6
    ws.cell(row=row_count, column=column_count, value="One size")

    # column_count = column_count + 27
    # ws.cell(row=row_count, column=column_count, value="One size")

    row_count = row_count + 1

    return products_list


def fill_FR_bags_sheet(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url):
    if product.static_search_tag_fr:
        search_tag = product.static_search_tag_fr
    else:
        search_tag = product.static_search_tag

    if product.title_fr:
        product_title = product.title_fr
    else:
        product_title = product.title

    if product.description_fr:
        product_description = product.description_fr
    else:
        product_description = product.description

    if product.type_fr:
        product_type = product.type_fr
    else:
        product_type = product.type_uk

    if product.departments_name_fr:
        departments_name = product.departments_name_fr
    else:
        departments_name = product.departments_name

    if product.material_composition_fr:
        material_composition = product.material_composition_fr
    else:
        material_composition = product.material_composition

    if product.bullet_point_1_fr:
        bullet_point_1 = product.bullet_point_1_fr
    else:
        bullet_point_1 = product.bullet_point_1

    if product.bullet_point_2_fr:
        bullet_point_2 = product.bullet_point_2_fr
    else:
        bullet_point_2 = product.bullet_point_2

    if product.bullet_point_3_fr:
        bullet_point_3 = product.bullet_point_3_fr
    else:
        bullet_point_3 = product.bullet_point_3

    if product.bullet_point_4_fr:
        bullet_point_4 = product.bullet_point_4_fr
    else:
        bullet_point_4 = product.bullet_point_4

    if product.bullet_point_5_fr:
        bullet_point_5 = product.bullet_point_5_fr
    else:
        bullet_point_5 = product.bullet_point_5

    row_count = ws.max_row + 1
    sku = account.sku_prefix + '-' + product.type_short + '-' + color_variation.color_name + '-' + sku_code

    column_count = 1
    global parent_sku
    parent_sku = sku
    ws.cell(row=row_count, column=column_count, value=product_type)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=account.brand_name)

    column_count = column_count + 3
    ws.cell(row=row_count, column=column_count, value=design.title + " " + product.title_fr)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.browse_node_fr)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=material_composition)

    column_count = column_count + 2
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_fr)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_fr)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.price_eur)

    # column_count = column_count + 1
    # ws.cell(row=row_count, column=column_count, value="GBP")

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=10)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value='http://' + host_name + generated_img_url)

    column_count = column_count + 1
    if product.extra_photo:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo.url)

    column_count = column_count + 1
    if product.extra_photo_2:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo_2.url)

    column_count = column_count + 13
    ws.cell(row=row_count, column=column_count, value=product_description)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 9
    ws.cell(row=row_count, column=column_count, value=bullet_point_1)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_2)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_3)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_4)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_5)

    search_tags = concat_search_tags(product.static_search_tag_fr, design.get_search_tags(), False)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=search_tags)

    column_count = column_count + 6
    ws.cell(row=row_count, column=column_count, value="One size")

    # column_count = column_count + 27
    # ws.cell(row=row_count, column=column_count, value="One size")

    row_count = row_count + 1

    return products_list


def fill_ES_bags_sheet(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url):
    if product.static_search_tag_es:
        search_tag = product.static_search_tag_es
    else:
        search_tag = product.static_search_tag

    if product.title_es:
        product_title = product.title_es
    else:
        product_title = product.title

    if product.description_es:
        product_description = product.description_es
    else:
        product_description = product.description

    if product.type_es:
        product_type = product.type_es
    else:
        product_type = product.type_uk

    if product.departments_name_es:
        departments_name = product.departments_name_es
    else:
        departments_name = product.departments_name

    if product.material_composition_es:
        material_composition = product.material_composition_es
    else:
        material_composition = product.material_composition

    if product.bullet_point_1_es:
        bullet_point_1 = product.bullet_point_1_es
    else:
        bullet_point_1 = product.bullet_point_1

    if product.bullet_point_2_es:
        bullet_point_2 = product.bullet_point_2_es
    else:
        bullet_point_2 = product.bullet_point_2

    if product.bullet_point_3_es:
        bullet_point_3 = product.bullet_point_3_es
    else:
        bullet_point_3 = product.bullet_point_3

    if product.bullet_point_4_es:
        bullet_point_4 = product.bullet_point_4_es
    else:
        bullet_point_4 = product.bullet_point_4

    if product.bullet_point_5_es:
        bullet_point_5 = product.bullet_point_5_es
    else:
        bullet_point_5 = product.bullet_point_5

    row_count = ws.max_row + 1
    sku = account.sku_prefix + '-' + product.type_short + '-' + color_variation.color_name + '-' + sku_code

    column_count = 1
    global parent_sku
    parent_sku = sku
    ws.cell(row=row_count, column=column_count, value=product_type)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=account.brand_name)

    column_count = column_count + 3
    ws.cell(row=row_count, column=column_count, value=design.title + " " + product.title_es)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.browse_node_es)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=material_composition)

    column_count = column_count + 2
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_es)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_es)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.price_eur)

    # column_count = column_count + 1
    # ws.cell(row=row_count, column=column_count, value="GBP")

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=10)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value='http://' + host_name + generated_img_url)

    column_count = column_count + 1
    if product.extra_photo:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo.url)

    column_count = column_count + 1
    if product.extra_photo_2:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo_2.url)

    column_count = column_count + 13
    ws.cell(row=row_count, column=column_count, value=product_description)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 15
    ws.cell(row=row_count, column=column_count, value=bullet_point_1)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_2)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_3)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_4)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_5)

    search_tags = concat_search_tags(product.static_search_tag_es, design.get_search_tags(), False)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=search_tags)

    column_count = column_count + 6
    ws.cell(row=row_count, column=column_count, value="One size")

    # column_count = column_count + 27
    # ws.cell(row=row_count, column=column_count, value="One size")

    row_count = row_count + 1

    return products_list


def fill_IT_bags_sheet(products_list, ws, product, color_variation, host_name, account, sku_code, design, generated_img_url):
    if product.static_search_tag_it:
        search_tag = product.static_search_tag_it
    else:
        search_tag = product.static_search_tag

    if product.title_it:
        product_title = product.title_it
    else:
        product_title = product.title

    if product.description_it:
        product_description = product.description_it
    else:
        product_description = product.description

    if product.type_it:
        product_type = product.type_it
    else:
        product_type = product.type_uk

    if product.departments_name_it:
        departments_name = product.departments_name_it
    else:
        departments_name = product.departments_name

    if product.material_composition_it:
        material_composition = product.material_composition_it
    else:
        material_composition = product.material_composition

    if product.bullet_point_1_it:
        bullet_point_1 = product.bullet_point_1_it
    else:
        bullet_point_1 = product.bullet_point_1

    if product.bullet_point_2_it:
        bullet_point_2 = product.bullet_point_2_it
    else:
        bullet_point_2 = product.bullet_point_2

    if product.bullet_point_3_it:
        bullet_point_3 = product.bullet_point_3_it
    else:
        bullet_point_3 = product.bullet_point_3

    if product.bullet_point_4_it:
        bullet_point_4 = product.bullet_point_4_it
    else:
        bullet_point_4 = product.bullet_point_4

    if product.bullet_point_5_it:
        bullet_point_5 = product.bullet_point_5_it
    else:
        bullet_point_5 = product.bullet_point_5

    row_count = ws.max_row + 1
    sku = account.sku_prefix + '-' + product.type_short + '-' + color_variation.color_name + '-' + sku_code

    column_count = 1
    # ws.cell(row=row_count, column=column_count, value=product.browse_node_it)

    global parent_sku
    parent_sku = sku
    ws.cell(row=row_count, column=column_count, value=product_type)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=account.brand_name)

    column_count = column_count + 3
    ws.cell(row=row_count, column=column_count, value=design.title + " " + product.title_es)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=product.browse_node_it)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=material_composition)

    column_count = column_count + 2
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_it)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.color_name_it)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=color_variation.price_eur)

    # column_count = column_count + 1
    # ws.cell(row=row_count, column=column_count, value="GBP")

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=10)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value='http://' + host_name + generated_img_url)

    column_count = column_count + 1
    if product.extra_photo:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo.url)

    column_count = column_count + 1
    if product.extra_photo_2:
        ws.cell(row=row_count, column=column_count, value='http://' + host_name + product.extra_photo_2.url)

    column_count = column_count + 13
    ws.cell(row=row_count, column=column_count, value=product_description)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=sku)

    column_count = column_count + 9
    ws.cell(row=row_count, column=column_count, value=bullet_point_1)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_2)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_3)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_4)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=bullet_point_5)

    search_tags = concat_search_tags(product.static_search_tag_it, design.get_search_tags(), False)

    column_count = column_count + 1
    ws.cell(row=row_count, column=column_count, value=search_tags)

    column_count = column_count + 6
    ws.cell(row=row_count, column=column_count, value="One size")

    # column_count = column_count + 27
    # ws.cell(row=row_count, column=column_count, value="One size")

    row_count = row_count + 1

    return products_list
