from __future__ import unicode_literals

# from amazoneexport.products.models import ColorVariation
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver

import os
from PIL import Image

# from templates_app.models import ProductTemplate
from django.conf import settings as project_settings
from . import utils
from .search_tags import get_search_tags


# Create your models here.

class Size(models.Model):
    long_name = models.CharField(max_length=255, blank=True)
    short_name = models.CharField(max_length=255, blank=True)
    order_nr = models.IntegerField(default=0)

    def __str__(self):
        return self.long_name


class ProductType(models.Model):
    name = models.CharField(blank=False, max_length=255)
    with_parent = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % (self.name)


class Country(models.Model):
    country_code = models.CharField(blank=False, max_length=255)

    def __str__(self):
        return "%s" % (self.country_code)


class Product_New(models.Model):
    TEMPLATE_CHOICES = (
        ('0', 'Clothing'),
        # ('1', 'Bag'),
        # ('2', 'Pillow'),
        ('3', 'Mugs'),
        # ('4', 'Hats'),
        # ('5', 'Stickers'),
        ('6', 'Home'),
        ('7', 'Luggage'),
    )

    #team = models.ForeignKey('accounts.Team', on_delete=models.CASCADE, default = 0)
    template = models.CharField(default="0", choices=TEMPLATE_CHOICES, max_length=1)
    # template_new = models.ForeignKey(ProductType, on_delete=models.CASCADE, default=1)
    team = models.ForeignKey('accounts.Team', on_delete=models.CASCADE, default=1)
    country = models.ForeignKey('Country', on_delete=models.CASCADE, default=1)
    title = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    meta_description = models.CharField(max_length=255, blank=True)
    type_full = models.CharField(max_length=255, blank=True) 
    type_short = models.CharField(max_length=255, blank=True)
    browse_node = models.CharField(max_length=255, blank=True)
    departments_name = models.CharField(max_length=255, blank=True)
    static_search_tags = models.CharField(max_length=200, blank=True)
    bullet_point_1 = models.CharField(max_length=1000, blank=True)
    bullet_point_2 = models.CharField(max_length=1000, blank=True)
    bullet_point_3 = models.CharField(max_length=1000, blank=True)
    bullet_point_4 = models.CharField(max_length=1000, blank=True)
    bullet_point_5 = models.CharField(max_length=1000, blank=True)
    material_composition = models.CharField(max_length=255, blank=True)
    item_weight = models.IntegerField(blank=True, null=True)
    parent_only = models.BooleanField(default=False, help_text="For hats, where only parent row is needed")
    sizes = models.ManyToManyField(Size)
    style_name = models.CharField(max_length=255, blank=True)
    size_name = models.CharField(max_length=255, blank=True)
    # liquid_volume = models.CharField(max_length=255, blank=True)
    # liquid_volume_unit_of_measure = models.CharField(max_length=255, blank=True)
    item_length = models.CharField(max_length=255, blank=True)
    item_length_unit_of_measure = models.CharField(max_length=255, blank=True)
    item_width = models.CharField(max_length=255, blank=True)
    item_width_unit_of_measure = models.CharField(max_length=255, blank=True)
    item_height = models.CharField(max_length=255, blank=True)
    item_height_unit_of_measure = models.CharField(max_length=255, blank=True)
    capacity = models.CharField(max_length=255, blank=True)
    capacity_unit_of_measure = models.CharField(max_length=255, blank=True)
    extra_photo = models.ImageField(upload_to='media', blank=True)
    extra_photo_2 = models.ImageField(upload_to='media', blank=True)
    extra_photo_3 = models.ImageField(upload_to='media', blank=True)
    extra_photo_4 = models.ImageField(upload_to='media', blank=True)
    extra_photo_5 = models.ImageField(upload_to='media', blank=True)

    def color_variations(self):
        color_vars = ColorVariation.objects.filter(product=self.id)
        return color_vars

    def __str__(self):  # __str__ for Python 3, __unicode__ for Python 2
        return self.title + ' (' + self.meta_description + ') ' + self.country.country_code


class Product(models.Model):
    TEMPLATE_CHOICES = (
        ('0', 'Clothing'),
        # ('1', 'Bag'),
        # ('2', 'Pillow'),
        ('3', 'Mugs'),
        # ('4', 'Hats'),
        # ('5', 'Stickers'),
        ('6', 'Home'),
        ('7', 'Luggage'),
    )

    #team = models.ForeignKey('accounts.Team', on_delete=models.CASCADE, default = 0)
    template = models.CharField(default="0", choices=TEMPLATE_CHOICES, max_length=1)
    # template_new = models.ForeignKey(ProductType, on_delete=models.CASCADE, default=1)
    team = models.ForeignKey('accounts.Team', on_delete=models.CASCADE, default=1)
    title = models.CharField(max_length=255, blank=True)
    title_de = models.CharField(max_length=255, blank=True)
    title_fr = models.CharField(max_length=255, blank=True)
    title_es = models.CharField(max_length=255, blank=True)
    title_it = models.CharField(max_length=255, blank=True)

    description = models.TextField(blank=True)
    description_de = models.TextField(blank=True)
    description_fr = models.TextField(blank=True)
    description_es = models.TextField(blank=True)
    description_it = models.TextField(blank=True)
    meta_description = models.CharField(max_length=255, blank=True)
    type_uk = models.CharField(max_length=255, blank=True)
    type_fr = models.CharField(max_length=255, blank=True)
    type_de = models.CharField(max_length=255, blank=True)
    type_com = models.CharField(max_length=255, blank=True)
    type_ca = models.CharField(max_length=255, blank=True)
    type_es = models.CharField(max_length=255, blank=True)
    type_it = models.CharField(max_length=255, blank=True)

    type_short = models.CharField(max_length=255, blank=True)
    browse_node_uk = models.CharField(max_length=255, blank=True)
    browse_node_de = models.CharField(max_length=255, blank=True)
    browse_node_fr = models.CharField(max_length=255, blank=True)
    browse_node_ca = models.CharField(max_length=255, blank=True)
    browse_node_jp = models.CharField(max_length=255, blank=True)
    browse_node_es = models.CharField(max_length=255, blank=True)
    browse_node_it = models.CharField(max_length=255, blank=True)

    departments_name = models.CharField(max_length=255, blank=True)
    departments_name_de = models.CharField(max_length=255, blank=True)
    departments_name_fr = models.CharField(max_length=255, blank=True)
    departments_name_es = models.CharField(max_length=255, blank=True)
    departments_name_it = models.CharField(max_length=255, blank=True)

    static_search_tags_1 = models.CharField(max_length=200, blank=True)
    static_search_tag_de = models.CharField(max_length=200, blank=True)
    static_search_tag_fr = models.CharField(max_length=200, blank=True)
    static_search_tag_es = models.CharField(max_length=200, blank=True)
    static_search_tag_it = models.CharField(max_length=200, blank=True)

    bullet_point_1 = models.CharField(max_length=1000, blank=True)
    bullet_point_2 = models.CharField(max_length=1000, blank=True)
    bullet_point_3 = models.CharField(max_length=1000, blank=True)
    bullet_point_4 = models.CharField(max_length=1000, blank=True)
    bullet_point_5 = models.CharField(max_length=1000, blank=True)

    bullet_point_1_de = models.CharField(max_length=1000, blank=True)
    bullet_point_2_de = models.CharField(max_length=1000, blank=True)
    bullet_point_3_de = models.CharField(max_length=1000, blank=True)
    bullet_point_4_de = models.CharField(max_length=1000, blank=True)
    bullet_point_5_de = models.CharField(max_length=1000, blank=True)

    bullet_point_1_fr = models.CharField(max_length=1000, blank=True)
    bullet_point_2_fr = models.CharField(max_length=1000, blank=True)
    bullet_point_3_fr = models.CharField(max_length=1000, blank=True)
    bullet_point_4_fr = models.CharField(max_length=1000, blank=True)
    bullet_point_5_fr = models.CharField(max_length=1000, blank=True)

    bullet_point_1_es = models.CharField(max_length=1000, blank=True)
    bullet_point_2_es = models.CharField(max_length=1000, blank=True)
    bullet_point_3_es = models.CharField(max_length=1000, blank=True)
    bullet_point_4_es = models.CharField(max_length=1000, blank=True)
    bullet_point_5_es = models.CharField(max_length=1000, blank=True)

    bullet_point_1_it = models.CharField(max_length=1000, blank=True)
    bullet_point_2_it = models.CharField(max_length=1000, blank=True)
    bullet_point_3_it = models.CharField(max_length=1000, blank=True)
    bullet_point_4_it = models.CharField(max_length=1000, blank=True)
    bullet_point_5_it = models.CharField(max_length=1000, blank=True)

    material_composition = models.CharField(max_length=255, blank=True)
    material_composition_de = models.CharField(max_length=255, blank=True)
    material_composition_fr = models.CharField(max_length=255, blank=True)
    material_composition_es = models.CharField(max_length=255, blank=True)
    material_composition_it = models.CharField(max_length=255, blank=True)

    fabric_type_uk = models.CharField(max_length=255, blank=True)
    fabric_type_de = models.CharField(max_length=255, blank=True)
    fabric_type_fr = models.CharField(max_length=255, blank=True)
    fabric_type_es = models.CharField(max_length=255, blank=True)
    fabric_type_it = models.CharField(max_length=255, blank=True)

    sleeve_type = models.CharField(max_length=255, blank=True)
    sleeve_type_de = models.CharField(max_length=255, blank=True)
    sleeve_type_fr = models.CharField(max_length=255, blank=True)
    sleeve_type_es = models.CharField(max_length=255, blank=True)
    sleeve_type_it = models.CharField(max_length=255, blank=True)

    item_weight = models.IntegerField(blank=True, null=True)

    parent_only = models.BooleanField(default=False, help_text="For hats, where only parent row is needed")

    sizes = models.ManyToManyField(Size)

    style_name = models.CharField(max_length=255, blank=True)
    size_name = models.CharField(max_length=255, blank=True)
    # liquid_volume = models.CharField(max_length=255, blank=True)
    # liquid_volume_unit_of_measure = models.CharField(max_length=255, blank=True)
    item_length = models.CharField(max_length=255, blank=True)
    item_length_unit_of_measure = models.CharField(max_length=255, blank=True)
    item_width = models.CharField(max_length=255, blank=True)
    item_width_unit_of_measure = models.CharField(max_length=255, blank=True)
    item_height = models.CharField(max_length=255, blank=True)
    item_height_unit_of_measure = models.CharField(max_length=255, blank=True)
    neck_size = models.CharField(max_length=255, blank=True)
    neck_size_unit_of_measure = models.CharField(max_length=255, blank=True)
    capacity = models.CharField(max_length=255, blank=True)
    capacity_unit_of_measure = models.CharField(max_length=255, blank=True)
    

    extra_photo = models.ImageField(upload_to='media', blank=True)
    extra_photo_2 = models.ImageField(upload_to='media', blank=True)
    extra_photo_3 = models.ImageField(upload_to='media', blank=True)
    extra_photo_4 = models.ImageField(upload_to='media', blank=True)
    extra_photo_5 = models.ImageField(upload_to='media', blank=True)

    def color_variations(self):
        color_vars = ColorVariation.objects.filter(product=self.id)
        return color_vars

    def __str__(self):  # __str__ for Python 3, __unicode__ for Python 2
        return self.title + ' (' + self.meta_description + ')'


class ColorVariation(models.Model):
    product = models.ForeignKey('Product', on_delete=models.CASCADE, null=True, blank=True)
    color_name = models.CharField(max_length=255)
    color_name_en = models.CharField(max_length=255, blank=True)
    color_name_de = models.CharField(max_length=255, blank=True)
    color_name_fr = models.CharField(max_length=255, blank=True)
    color_name_es = models.CharField(max_length=255, blank=True)
    color_name_it = models.CharField(max_length=255, blank=True)
    color_name_short = models.CharField(max_length=255, blank=True)
    price_usd = models.FloatField(blank=True, default=0)
    price_eur = models.FloatField(blank=True, default=0)
    price_gbp = models.FloatField(blank=True, default=0)
    price_cad = models.FloatField(blank=True, default=0)
    image = models.ImageField(upload_to='templates', blank=True)
    opacity = models.FloatField(blank=True, default=0)
    upper_x = models.IntegerField(default=0)
    upper_y = models.IntegerField(default=0)
    lower_x = models.IntegerField(default=0)
    lower_y = models.IntegerField(default=0)
    parent = models.BooleanField(default=False)

    def __str__(self):  # __str__ for Python 3, __unicode__ for Python 2
        return self.color_name + ' (' + self.product.title + ')'


class Design(models.Model):
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    docfile = models.FileField(upload_to='designs')
    filename = models.CharField(blank=True, max_length=255)
    title = models.CharField(blank=True, max_length=255)
    search_tags = models.CharField(max_length=50, blank=True, null=True)
    search_tags_1 = models.CharField(blank=True, max_length=1000)
    search_tags_2 = models.CharField(blank=True, max_length=1000)
    search_tags_3 = models.CharField(blank=True, max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    sku_code = models.CharField(blank=True, max_length=255)

    def get_thumbnail(self):
        thumbnail = TiffThumbnails.objects.filter(tiff=self)
        if thumbnail:
            return thumbnail[0].thumbnail_url
        else:
            # cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__))) + "/temp/dizai/"

            img_url = ""
            try:
                filename = os.path.basename(self.docfile.name).replace(".tif", "")

                img_url = "/media/thumbnails/" + filename + ".jpg"
                outfile = os.path.abspath(os.path.dirname(os.path.dirname(__file__))) + "/public" + img_url

                im = Image.open(self.docfile)
                size = 300, 300
                im.thumbnail(size)
                im.save(outfile, "JPEG", quality=100)

                thumbnailas = TiffThumbnails(tiff=self, thumbnail_url=img_url)
                thumbnailas.save()
            except:
                pass

            return img_url

    def scrape_search_tags(self, search_query):
        try:
            search_tags_result = " ".join(get_search_tags(search_query))
            self.search_tags = search_tags_result[0:50]
            self.save()
        except Exception as e:
            print(e)
            pass

    def get_search_tags(self):
        if self.search_tags is None:
            str = " ".join([self.search_tags_1, self.search_tags_2, self.search_tags_3])
            return utils.slice_string(str, 50)
        else:
            return self.search_tags

    def __str__(self):
        return "%s %s" % (self.sku_code, self.title)


class GeneratedDesigns(models.Model):
    docfile = models.ImageField(upload_to='generated-images')
    filename = models.CharField(blank=True, max_length=255)
    imgurl = models.CharField(blank=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    generation = models.ForeignKey('generation', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % self.id


@receiver(pre_delete, sender=GeneratedDesigns, dispatch_uid='generated_design_pre_delete')
def delete_generated_design_docfile(sender, instance, using, **kwargs):
    base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # TODO: refactor to proper paths..

    try:
        os.remove(os.path.join(base_path, "public" + str(instance.docfile.url)))
    except Exception as e:
        pass


# Sequence
class DesignGeneration(models.Model):
    design = models.ForeignKey('design', on_delete=models.CASCADE, default=0)
    code = models.CharField(blank=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    # generation = models.ForeignKey('generation', blank=True, null=True)

    def __str__(self):
        return "%s" % self.code


class GeneratedFile(models.Model):
    generation = models.ForeignKey('generation', on_delete=models.CASCADE)
    file_path = models.CharField(blank=True, max_length=255)
    full_file_path = models.CharField(blank=True, max_length=255)
    file_name = models.CharField(blank=True, max_length=255)
    market = models.CharField(blank=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.file_path


class Generation(models.Model):
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    total_rows = models.IntegerField(default=0)
    completed_rows = models.IntegerField(default=0)
    failed_rows = models.IntegerField(default=0)
    generated = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    elapsed_time = models.IntegerField(default=0)

    def noname_files(self):
        q = GeneratedFile.objects.filter(generation=self, market="")
        return q

    def uk_files(self):
        q = GeneratedFile.objects.filter(generation=self, market="UK")
        return q

    def de_files(self):
        q = GeneratedFile.objects.filter(generation=self, market="DE")
        return q

    def fr_files(self):
        q = GeneratedFile.objects.filter(generation=self, market="FR")
        return q

    def es_files(self):
        q = GeneratedFile.objects.filter(generation=self, market="ES")
        return q

    def it_files(self):
        q = GeneratedFile.objects.filter(generation=self, market="IT")
        return q

    def failed_designs(self):
        return GenerationError.objects.filter(generation=self)

    def __str__(self):
        return "%s %s" % (self.id, self.created_at)


class GenerationError(models.Model):
    generation = models.ForeignKey('Generation', on_delete=models.CASCADE)
    design = models.ForeignKey('Design', on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % self.id


class SkuCodes(models.Model):
    color_variation = models.ForeignKey('ColorVariation', on_delete=models.CASCADE, default=0)
    code = models.CharField(blank=True, max_length=255)
    design = models.ForeignKey('Design', on_delete=models.CASCADE, default=0)
    img_url = models.CharField(blank=True, max_length=255)

    generation = models.ForeignKey('generation', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (self.id, self.img_url)


class TiffThumbnails(models.Model):
    tiff = models.ForeignKey('Design', on_delete=models.CASCADE, default=0)
    thumbnail_url = models.CharField(blank=True, max_length=255)

    def __str__(self):
        return "%s" % (self.id)


class AccountDesign(models.Model):
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE, default=0)
    design = models.ForeignKey('Design', on_delete=models.CASCADE, default=0)

    def __str__(self):
        return "%s" % (self.id)



