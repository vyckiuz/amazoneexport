from __future__ import division
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.static import serve
from django.db.models import Q
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
import zipfile
import shutil
import glob
import zipfile
import sys

from products.models import Product, Design
from templates_app.models import ProductTemplate, Language
from .forms import UploadFileForm, MassUploadForm, DesignDownloadForm, MassDownloadForm
import os
import shutil
from django.conf import settings as proj_settings

from openpyxl import load_workbook
from .models import ColorVariation, GeneratedDesigns, DesignGeneration, GeneratedFile, Generation, SkuCodes, GenerationError, AccountDesign
from accounts.models import Account, TeamUser
import string
import random
import datetime
from PIL import Image
from django.core.files import File
import threading
from .clothing_templates import fill_UK_sheet, fill_DE_sheet, fill_FR_sheet, fill_ES_sheet, fill_IT_sheet
from .mugs_templates_generation import fill_mugs_sheet
from .bags_templates_generation import fill_UK_bags_sheet, fill_DE_bags_sheet, fill_FR_bags_sheet, fill_ES_bags_sheet, fill_IT_bags_sheet
from .luggage_templates_generation import fill_luggage_sheet
from .search_tags import get_search_tags
import time

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import psycopg2
from .old_db import OldDB


@login_required
def index(request):
    if request.method == "POST":
        form = DesignDownloadForm(request.POST)

        if form.is_valid():
            sku = form.cleaned_data['sku']
            design = get_object_or_404(Design, sku_code=sku)
            ext = os.path.splitext(os.path.basename(design.docfile.name))[1]
            new_file_name = sku.upper() + ext

            # filename = design.docfile.file.name.split('/')[-1]
            response = HttpResponse(design.docfile, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename=%s' % new_file_name

            return response
    else:
        form = DesignDownloadForm()

    context = {
        "form": form
    }

    return render(request, "index.html", context)


# @login_required
# def index(request):
#     cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
#     with open(cwd + "/no_images.txt", "a") as no_images_file:
#         no_images_file.write("Tests")
#         no_images_file.close()
#
#     context = {}
#     if request.method == 'POST':
#         sku = request.POST['sku_id']
#         context['sku'] = sku
#         try:
#             design = Design.objects.filter(sku_code__iexact=sku).first()
#             account = design.account
#             user_team = TeamUser.objects.filter(user=request.user)
#             if user_team:
#                 user_team = user_team.first()
#                 if account.team.id == user_team.team.id:
#                     context['design'] = design
#
#                     # download the design file
#
#                 else:
#                     context['message'] = 'Error1'
#             else:
#                 context['message'] = 'Error2'
#         except:
#             context['message'] = 'SKU doesnt exist'
#
#     return render(request, 'index.html', context)


@login_required
def generated_files(request):
    context = {}
    # account
    user_team = TeamUser.objects.filter(user=request.user)
    if user_team:
        user_team = user_team.first()
        accounts = Account.objects.filter(team=user_team.team)
        generations = Generation.objects.filter(account__in=accounts).order_by('-pk')[0:10]
        context['generations'] = generations
    return render(request, 'generated_files.html', context)


@login_required
def generate(request):
    if request.method == 'POST':
        variations = request.POST.getlist('variations')
        account_id = request.POST['accounts1']
        account = Account.objects.get(pk=account_id)

        variations_q = ColorVariation.objects.filter(pk__in=variations).order_by('-parent')

        my_args = []
        my_args.append(variations_q)
        my_args.append(request.META['HTTP_HOST'])
        my_args.append(account)
        my_args.append(request.POST['date_from'])

        function_name = fill_excel

        excel_thread = threading.Thread(target=function_name, args=my_args)
        excel_thread.start()

        return redirect('generated_files')
        # return generated_files(request) #render(request, 'generated_files.html', context)
        # return serve(request, os.path.basename(file_dir), os.path.dirname(file_dir))
    # return render(request, 'generate.html', context)
    else:
        team = TeamUser.objects.filter(user=request.user)
        if team:
            team = team.first().team
            accounts = Account.objects.filter(team=team)
            products = Product.objects.filter(team=team)
        else:
            accounts = None
            designs = None  # Design.objects.all()
            products = None

        context = {}
        # accounts = Account.objects.all()
        # products = Product.objects.all()
        context['products'] = products
        context['accounts'] = accounts
        return render(request, 'generate.html', context)


@login_required
def upload(request):
    designs_with_sku = Design.objects.exclude(sku_code="")
    if designs_with_sku:
        code = increment_str(designs_with_sku.last().sku_code)
    else:
        code = 'DBAAA'

    designs = Design.objects.filter(sku_code="")
    for design in designs:
        design.sku_code = code
        design.save()
        code = increment_str(code)

    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)

        files = request.FILES.getlist('file')
        title = request.POST['title']
        account_id = request.POST['accounts1']
        account = Account.objects.get(pk=account_id)
        counter = 0

        search_tags = request.POST['search_tags']

        for f in files:
            search_query = None

            if title:
                title2 = title
            else:
                filename = os.path.splitext(f.name)[0]

                if "-" in filename:
                    title2 = filename.split("-")[1].strip()
                    search_query = filename.split("-")[0].strip()
                else:
                    title2 = filename

            f.name = f.name.replace(' ', '_')

            # create design
            design = Design(filename=f.name, docfile=f, title=title2, search_tags=search_tags, account=account, sku_code=code)

            # If search tags not present - scrape search tags
            if search_tags.strip() == "":
                try:
                    if search_query is None:
                        if "search_query" in request.POST and request.POST['search_query'].strip() != "":
                            search_query = request.POST["search_query"]
                        else:
                            search_query = title2

                    new_t = threading.Thread(target=design.scrape_search_tags, args=(search_query,))
                    new_t.start()
                except Exception as e:
                    pass

            design.save()
            code = increment_str(code)

        form = UploadFileForm()
        team = TeamUser.objects.filter(user=request.user)
        if team:
            team = team.first().team
            accounts = Account.objects.filter(team=team)
        else:
            accounts = None

        # return render(request, 'upload.html', {'form': form})
        response = redirect('upload')
        response['Location'] += '?account_id=%s' % account.pk
        return response
    else:
        team = TeamUser.objects.filter(user=request.user)
        if team:
            team = team.first().team
            accounts = Account.objects.filter(team=team)
        else:
            accounts = None

        account_id = request.GET.get("account_id")
        if account_id is not None:
            account = get_object_or_404(Account, pk=account_id)
        else:
            account = Account.objects.all().first()

        form = UploadFileForm(initial={"accounts1": account})

    return render(request, 'upload.html', {'form': form, 'accounts': accounts})


# def handle_mass_upload_zip(path, account):
#     directory_path = os.path.dirname(path)
#
#     zip_ref = zipfile.ZipFile(path, 'r')
#     zip_ref.extractall(directory_path)
#     zip_ref.close()
#
#     os.remove(path)
#     design_paths = []
#
#     for filename in os.listdir(directory_path):
#         file_path = os.path.join(directory_path, filename)
#
#         if os.path.isfile(file_path):
#             base_name = os.path.basename(file_path).split(".")[0]
#
#             if "-" in base_name:
#                 search_query = base_name.split("-")[0].strip()
#                 title = base_name.split("-")[1].strip()
#             else:
#                 search_query = base_name
#                 title = base_name
#
#             search_tags = ""
#             if search_query:
#                 try:
#                     search_tags_result = get_search_tags(search_query)
#                     search_tags = ",".join(search_tags_result)
#                 except Exception as e:
#                     print e
#
#             designs_with_sku = Design.objects.exclude(sku_code="")
#             if designs_with_sku:
#                 code = increment_str(designs_with_sku.last().sku_code)
#
#             print search_query
#             print title
#             print search_tags
#             print code
#             print account
#
#             print filename
#
#             # design = Design(filename=filename, title=title2, search_tags=search_tags, account=account, sku_code=code)
#
#             print file_path
#             os.remove(file_path)
#         # else:
#         #     print "LOL"
#         #     print file_path


@login_required
def mass_upload(request):
    if request.method == "POST":
        form = MassUploadForm(request.POST, request.FILES)

        if form.is_valid():
            myfile = request.FILES['file']
            fs = FileSystemStorage()
            filename = fs.save('mass-upload-zips/' + myfile.name, myfile)
            uploaded_file_url = fs.url(filename)

            zip_path = os.path.join(proj_settings.BASE_DIR, "../public/media/mass-upload-zips", myfile.name)
            # handle_mass_upload_zip(zip_path, form.cleaned_data['account'])
            # print zip_path

            return redirect('mass_upload')
    else:
        form = MassUploadForm()

    context = {
        "form": form,
    }

    return render(request, "mass_upload.html", context)


@login_required
def designs(request):
    search_title = ""
    pagination_number = 20
    try:
        if request.GET.get('designs_per_page'):
            pagination_number = int(request.GET.get('designs_per_page'))
            request.session['designs_per_page'] = request.GET.get('designs_per_page')
        elif 'designs_per_page' in request.session:
            pagination_number = request.session['designs_per_page']
    except:
        pass

    team = TeamUser.objects.filter(user=request.user)
    show_pages = True
    page = request.GET.get('page')
    account_text = ""
    if team:
        team = team.first().team
        team_accounts = Account.objects.filter(team=team)
        if request.GET.get('account') and request.GET.get('account').lower() != "all accounts":
            accounts = Account.objects.filter(team=team).filter(id=request.GET.get('account'))
            account_text = "&account=" + request.GET.get('account')
        else:
            accounts = Account.objects.filter(team=team)

        designs_ids = []
        account_designs = AccountDesign.objects.filter(account__in=accounts)
        for account_design in account_designs:
            designs_ids.append(account_design.design.id)

        designs_list = Design.objects.filter(account__in=accounts) | Design.objects.filter(id__in=designs_ids)
        designs_list = designs_list.order_by('id')
        # account_id = ""
        # if 'search_design' in request.GET:
        if request.GET.get('search_title'):
            search_title = request.GET.get('search_title')

            designs_list = designs_list.filter(Q(title__icontains=search_title) | Q(sku_code__icontains=search_title))
            account_text += "&search_title=" + search_title

        if page == "show_all":
            designs = designs_list
            show_pages = False
        else:
            paginator = Paginator(designs_list, pagination_number)
            try:
                designs = paginator.page(page)
            except PageNotAnInteger:
                designs = paginator.page(1)
            except EmptyPage:
                designs = paginator.page(paginator.num_pages)
    else:
        accounts = None
        designs = None  # Design.objects.all()
    return render(request, 'designs.html', {'designs': designs, 'show_pages': show_pages, 'team_accounts': team_accounts, 'account_text': account_text, 'pagination_number': pagination_number, 'search_title': search_title})


@login_required
def edit_design(request, design_id):
    design = Design.objects.get(id=design_id)
    return render(request, 'edit_design.html', {'design': design})


@login_required
def update_design(request, design_id):
    design = Design.objects.get(id=design_id)

    if request.method == 'POST':
        title = request.POST['title']
        # search_tags_1 = request.POST['search_tags_1']
        # search_tags_2 = request.POST['search_tags_2']
        # search_tags_3 = request.POST['search_tags_3']

        design.title = title
        design.search_tags = request.POST['search_tags'][0:50]

        design.save()

    return redirect('designs')


@login_required
def failed_designs(request, generation_id):
    generation = Generation.objects.get(id=generation_id)
    generation_errors = GenerationError.objects.filter(generation=generation)

    return render(request, 'generation_errors.html', {'errors': generation_errors})
    # return HttpResponse(generation_id)


@login_required
def assign_accounts(request):
    if request.method == "POST":
        if "to_account" in request.POST:
            account = Account.objects.get(id=request.POST["to_account"])
            designs = request.POST.getlist('designs_checkboxes')
            if "move_designs" in request.POST:
                for design in designs:
                    design_object = Design.objects.get(pk=design)
                    design_object.account = account
                    design_object.save()

            elif "copy_designs" in request.POST:
                for design in designs:
                    design_object = Design.objects.get(pk=design)
                    account_design = AccountDesign(account=account, design=design_object)
                    account_design.save()
    return redirect(request.META.get('HTTP_REFERER'))


def get_file_dir(product_type, account_name, generation_id, market, product):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    data_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/public/media/generated-files/" + account_name + "/" + today_date + "/" + product_type + "/" + generation_id + "/"
    header_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/public/headers/" + product_type.lower()  # os.path.join(os.getcwd(), "public/headers/Clothing")
    if not os.path.isdir(data_directory):
        os.makedirs(data_directory)

    file_name = "%s-%s-%s" % (product.title, market, generation_id)
    file_dir = data_directory + "/" + file_name + ".xlsx"
    if not os.path.isfile(file_dir):
        shutil.copy2(header_directory + "/" + market + ".xlsx", file_dir)

    return file_dir


def fill_excel(variations, host_name, account, from_date):
    # for variation in variations:
    # color_variation = ColorVariation.objects.get(pk = variation)

    generation = Generation(account=account, total_rows=0)
    generation.save()

    products_list = []
    for variation in variations:
        if variation.product not in products_list:
            products_list.append(variation.product)

    for product in products_list:
        if product.template == "0":
            fill_excel_clothing(variations, host_name, account, from_date, "clothing", product, generation)

        elif product.template == "3":
            fill_excel_mugs(variations, host_name, account, from_date, "mugs", product, generation)

        elif product.template == "1":
            fill_excel_bags(variations, host_name, account, from_date, "bags", product, generation)

        elif product.template == "7":
            fill_excel_luggage(variations, host_name, account, from_date, "luggage", product, generation)


def fill_sheet(ws, template_fields, all_fields, account, product_list_sheet):
    sku_code = all_fields["item_sku"]
    row_count = ws.max_row + 1
    if all_fields["product"].template_new.with_parent:
        not_neccessary_list = ["id", "product", "language", "parent_sku", "relationship_type", "color_map", "color_name", "size_map", "size_name", "material_composition", "outer_material_type", "department_name", "is_adult_product"]
        if not all_fields["product"].id in product_list_sheet:
            if all_fields["variation"].parent:
                product_list_sheet.append(all_fields["product"].id)
            all_fields["item_sku"] = all_fields["sku_prefix"] + '-' + all_fields["type_short"] + '-' + all_fields["color_name"] + '-' + sku_code
            # all_fields["parent_sku"] = all_fields["item_sku"]
            all_fields["part_number"] = all_fields["item_sku"]
            all_fields["parent_child"] = "parent"

            for field in template_fields._meta.fields:
                field = str(field).replace("templates_app.ProductTemplate.", "")
                if field not in not_neccessary_list:
                    if field in all_fields:
                        column_letter = getattr(template_fields, field)
                        if column_letter:
                            int_column = col2num(column_letter)
                            ws.cell(row=row_count, column=int_column, value=all_fields[field])

    not_neccessary_list = ["id", "product", "language"]
    all_fields["parent_child"] = "child"
    all_fields["parent_sku"] = all_fields["sku_prefix"] + '-' + all_fields["type_short"] + '-' + all_fields["color_name"] + '-' + sku_code
    for size in all_fields["product"].sizes.all().order_by('order_nr'):
        row_count = ws.max_row + 1
        all_fields["size_map"] = size.long_name
        all_fields["size_name"] = size.long_name
        all_fields["item_sku"] = all_fields["sku_prefix"] + '-' + all_fields["type_short"] + '-' + all_fields["color_name"] + '-' + size.short_name + '-' + sku_code
        all_fields["part_number"] = all_fields["item_sku"]
        for field in template_fields._meta.fields:
            field = str(field).replace("templates_app.ProductTemplate.", "")
            if field not in not_neccessary_list:
                if field in all_fields:
                    column_letter = getattr(template_fields, field)
                    if column_letter:
                        int_column = col2num(column_letter)
                        ws.cell(row=row_count, column=int_column, value=all_fields[field])

    return product_list_sheet


def language_fields(all_fields, market, product, variation, design):
    if market == "UK":
        all_fields["product_subtype"] = product.type_uk
        all_fields["product_description"] = product.description
        all_fields["standard_price"] = variation.price_gbp
        all_fields["recommended_browse_nodes"] = product.browse_node_uk
        all_fields["recommended_browse_nodes1"] = product.browse_node_uk
        all_fields["bullet_point1"] = product.bullet_point_1
        all_fields["bullet_point2"] = product.bullet_point_2
        all_fields["bullet_point3"] = product.bullet_point_3
        all_fields["bullet_point4"] = product.bullet_point_4
        all_fields["bullet_point5"] = product.bullet_point_5
        all_fields["country_of_origin"] = "Lithuania"
        all_fields["color_map"] = variation.color_name_en
        all_fields["color_name"] = variation.color_name_en
        all_fields["material_composition"] = product.material_composition
        all_fields["outer_material_type"] = product.material_composition
        all_fields["material_type"] = product.material_composition
        all_fields["inner_material_type"] = product.material_composition
        all_fields["department_name"] = product.departments_name
        all_fields["currency"] = "GBP"
        all_fields["feed_product_type"] = product.type_uk
        all_fields["generic_keywords1"] = product.static_search_tag
        all_fields["generic_keywords2"] = design.search_tags_1
        all_fields["generic_keywords3"] = design.search_tags_2
        all_fields["generic_keywords4"] = design.search_tags_3
        all_fields["generic_keywords5"] = ""
        all_fields["generic_keywords"] = product.static_search_tag
    elif market == "DE":
        all_fields["product_subtype"] = product.type_de
        all_fields["product_description"] = product.description_de
        all_fields["standard_price"] = variation.price_eur
        all_fields["recommended_browse_nodes"] = product.browse_node_de
        all_fields["recommended_browse_nodes1"] = product.browse_node_de
        all_fields["bullet_point1"] = product.bullet_point_1_de
        all_fields["bullet_point2"] = product.bullet_point_2_de
        all_fields["bullet_point3"] = product.bullet_point_3_de
        all_fields["bullet_point4"] = product.bullet_point_4_de
        all_fields["bullet_point5"] = product.bullet_point_5_de
        all_fields["country_of_origin"] = "Litauen"
        all_fields["color_map"] = variation.color_name_de
        all_fields["color_name"] = variation.color_name_de
        all_fields["material_composition"] = product.material_composition_de
        all_fields["outer_material_type"] = product.material_composition_de
        all_fields["material_type"] = product.material_composition_de
        all_fields["inner_material_type"] = product.material_composition_de
        all_fields["department_name"] = product.departments_name_de
        all_fields["currency"] = "EUR"
        all_fields["feed_product_type"] = product.type_de
        all_fields["generic_keywords1"] = product.static_search_tag_de
        all_fields["generic_keywords2"] = design.search_tags_1
        all_fields["generic_keywords3"] = design.search_tags_2
        all_fields["generic_keywords4"] = design.search_tags_3
        all_fields["generic_keywords5"] = ""
        all_fields["generic_keywords"] = product.static_search_tag_de
    elif market == "FR":
        all_fields["product_subtype"] = product.type_fr
        all_fields["product_description"] = product.description_fr
        all_fields["standard_price"] = variation.price_eur
        all_fields["recommended_browse_nodes"] = product.browse_node_fr
        all_fields["recommended_browse_nodes1"] = product.browse_node_fr
        all_fields["bullet_point1"] = product.bullet_point_1_fr
        all_fields["bullet_point2"] = product.bullet_point_2_fr
        all_fields["bullet_point3"] = product.bullet_point_3_fr
        all_fields["bullet_point4"] = product.bullet_point_4_fr
        all_fields["bullet_point5"] = product.bullet_point_5_fr
        all_fields["country_of_origin"] = "Lituanie"
        all_fields["color_map"] = variation.color_name_fr
        all_fields["color_name"] = variation.color_name_fr
        all_fields["material_composition"] = product.material_composition_fr
        all_fields["outer_material_type"] = product.material_composition_fr
        all_fields["material_type"] = product.material_composition_fr
        all_fields["inner_material_type"] = product.material_composition_fr
        all_fields["department_name"] = product.departments_name_fr
        all_fields["currency"] = "EUR"
        all_fields["feed_product_type"] = product.type_fr
        all_fields["generic_keywords1"] = product.static_search_tag_fr
        all_fields["generic_keywords2"] = design.search_tags_1
        all_fields["generic_keywords3"] = design.search_tags_2
        all_fields["generic_keywords4"] = design.search_tags_3
        all_fields["generic_keywords5"] = ""
        all_fields["generic_keywords"] = product.static_search_tag_fr

    elif market == "ES":
        all_fields["product_subtype"] = product.type_es
        all_fields["product_description"] = product.description_es
        all_fields["standard_price"] = variation.price_eur
        all_fields["recommended_browse_nodes"] = product.browse_node_es
        all_fields["recommended_browse_nodes1"] = product.browse_node_es
        all_fields["bullet_point1"] = product.bullet_point_1_es
        all_fields["bullet_point2"] = product.bullet_point_2_es
        all_fields["bullet_point3"] = product.bullet_point_3_es
        all_fields["bullet_point4"] = product.bullet_point_4_es
        all_fields["bullet_point5"] = product.bullet_point_5_es
        all_fields["country_of_origin"] = "Lituania"
        all_fields["color_map"] = variation.color_name_es
        all_fields["color_name"] = variation.color_name_es
        all_fields["material_composition"] = product.material_composition_es
        all_fields["outer_material_type"] = product.material_composition_es
        all_fields["material_type"] = product.material_composition_es
        all_fields["inner_material_type"] = product.material_composition_es
        all_fields["department_name"] = product.departments_name_es
        all_fields["currency"] = "EUR"
        all_fields["feed_product_type"] = product.type_es
        all_fields["generic_keywords1"] = product.static_search_tag_es
        all_fields["generic_keywords2"] = design.search_tags_1
        all_fields["generic_keywords3"] = design.search_tags_2
        all_fields["generic_keywords4"] = design.search_tags_3
        all_fields["generic_keywords5"] = ""
        all_fields["generic_keywords"] = product.static_search_tag_es

    elif market == "IT":
        all_fields["product_subtype"] = product.type_it
        all_fields["product_description"] = product.description_it
        all_fields["standard_price"] = variation.price_eur
        all_fields["recommended_browse_nodes"] = product.browse_node_it
        all_fields["recommended_browse_nodes1"] = product.browse_node_it
        all_fields["bullet_point1"] = product.bullet_point_1_it
        all_fields["bullet_point2"] = product.bullet_point_2_it
        all_fields["bullet_point3"] = product.bullet_point_3_it
        all_fields["bullet_point4"] = product.bullet_point_4_it
        all_fields["bullet_point5"] = product.bullet_point_5_it
        all_fields["country_of_origin"] = "Lituania"
        all_fields["color_map"] = variation.color_name_it
        all_fields["color_name"] = variation.color_name_it
        all_fields["material_composition"] = product.material_composition_it
        all_fields["outer_material_type"] = product.material_composition_it
        all_fields["material_type"] = product.material_composition_it
        all_fields["inner_material_type"] = product.material_composition_it
        all_fields["department_name"] = product.departments_name_it
        all_fields["currency"] = "EUR"
        all_fields["feed_product_type"] = product.type_it
        all_fields["generic_keywords1"] = product.static_search_tag_it
        all_fields["generic_keywords2"] = design.search_tags_1
        all_fields["generic_keywords3"] = design.search_tags_2
        all_fields["generic_keywords4"] = design.search_tags_3
        all_fields["generic_keywords5"] = ""
        all_fields["generic_keywords"] = product.static_search_tag_it

    return all_fields


def fill_excel_new(variations, host_name, account, from_date):
    global products_list_global
    products_list_global = []
    cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    languages = ["UK", "DE", "FR", "ES", "IT"]

    designs_ids = []
    account_designs = AccountDesign.objects.filter(account=account)
    for account_design in account_designs:
        designs_ids.append(account_design.design.id)

    designs = Design.objects.filter(account=account) | Design.objects.filter(id__in=designs_ids)
    if from_date:
        designs = designs.filter(created_at__gte=from_date)

    products_list = []
    for variation in variations:
        if variation.product in products_list:
            continue

        start = time.time()
        product = variation.product
        variation_product = product
        products_list.append(product)
        product_template_obj = product.template_new
        generation = Generation(account=account, total_rows=len(designs))
        generation.save()

        file_dirs = {}
        wb = {}
        ws = {}
        template_fields_dic = {}

        for language in languages:
            language_obj = Language.objects.filter(short_name=language).first()
            if language_obj:
                template_fields = ProductTemplate.objects.filter(product=product_template_obj).filter(language=language_obj).first()
                if template_fields:
                    template_fields_dic[language] = template_fields
                    file_dirs[language] = get_file_dir(template_fields.product.name, account.brand_name, str(generation.id), language)
                    wb[language] = load_workbook(file_dirs[language])
                    ws[language] = wb[language].worksheets[0]

        designs_number = 0
        for design in designs:
            product_list_sheet = {}
            for lang in languages:
                product_list_sheet[lang] = []
            try:
                designs_number = designs_number + 1
                if design.sku_code:
                    sku_code = design.sku_code
                else:
                    designs_with_sku = Design.objects.exclude(sku_code="")
                    if designs_with_sku:
                        code = increment_str(designs_with_sku.last().sku_code)
                    else:
                        code = 'DBAAA'

                    design.sku_code = code
                    design.save()

                    sku_code = code

                for variation in variations:
                    if variation.product == variation_product:
                        sku_object = SkuCodes.objects.filter(design=design).filter(color_variation=variation).first()
                        if sku_object:
                            generated_img_url = sku_object.img_url
                        else:
                            generated_img_url = image_generation(variation, design.docfile.url)
                            sku_object = SkuCodes(color_variation=variation, design=design, code=sku_code, img_url=generated_img_url)
                            sku_object.save()

                        file_name = product.title

                        for market in ws:
                            all_fields = {}
                            all_fields["item_sku"] = sku_code
                            all_fields["item_name"] = design.title + " " + product.title
                            all_fields["brand_name"] = account.brand_name
                            all_fields["quantity"] = "10"
                            all_fields["main_image_url"] = "http://" + host_name + generated_img_url
                            if product.extra_photo:
                                all_fields["other_image_url1"] = 'http://' + host_name + product.extra_photo.url
                            if product.extra_photo_2:
                                all_fields["other_image_url2"] = 'http://' + host_name + product.extra_photo_2.url
                            all_fields["relationship_type"] = "variation"
                            all_fields["variation_theme"] = "size-color"
                            all_fields["is_adult_product"] = "true"
                            all_fields["manufacturer"] = account.brand_name
                            all_fields["sku_prefix"] = account.sku_prefix
                            all_fields["type_short"] = product.type_short
                            all_fields["product"] = product
                            all_fields["variation"] = variation

                            all_fields = language_fields(all_fields, market, product, variation, design)

                            product_list_sheet[market] = fill_sheet(ws[market], template_fields_dic[market], all_fields, account, product_list_sheet[market])

                generation.completed_rows = designs_number
                generation.save()
            except Exception as e:
                with open(cwd + "/errors.txt", "a") as output_file:
                    output_file.write("1. " + str(e) + ". Design id: " + str(design.id) + "\n")
                    output_file.close()
                generation.failed_rows = generation.failed_rows + 1
                generation.save()

                generationError = GenerationError(generation=generation, design=design)
                generationError.save()

        for market in wb:
            wb[market].save(file_dirs[market])
            file_path = file_dirs[market]
            position = file_path.index('/media/')
            file_path = file_path[position:]

            generated_file = GeneratedFile(generation=generation, full_file_path=file_dirs[market], file_path=file_path, file_name=file_name, market=market)
            generated_file.save()

        end = time.time()
        generation.generated = True
        generation.elapsed_time = end - start
        generation.save()

    return HttpResponse("Done")


def fill_excel_clothing(variations, host_name, account, from_date, product_template, variation_product, generation):
    cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    file_name = ""
    start = time.time()

    designs_ids = []
    account_designs = AccountDesign.objects.filter(account=account)
    for account_design in account_designs:
        designs_ids.append(account_design.design.id)

    designs = Design.objects.filter(account=account) | Design.objects.filter(id__in=designs_ids)

    if from_date:
        designs = designs.filter(created_at__gte=from_date)

    markets = ["UK", "DE", "FR", "ES", "IT"]
    # markets = ["DE", "FR"]
    # generation = Generation(account = account, total_rows = len(designs))
    generation.total_rows = len(designs)
    generation.save()

    file_dir_uk = get_file_dir(product_template, account.brand_name, str(generation.id), "UK", variation_product)  # color_variation.template
    wb_uk = load_workbook(file_dir_uk)
    ws_uk = wb_uk.worksheets[0]
    # print ws_uk

    file_dir_de = get_file_dir(product_template, account.brand_name, str(generation.id), "DE", variation_product)  # color_variation.template
    wb_de = load_workbook(file_dir_de)
    ws_de = wb_de.worksheets[0]

    file_dir_fr = get_file_dir(product_template, account.brand_name, str(generation.id), "FR", variation_product)  # color_variation.template
    wb_fr = load_workbook(file_dir_fr)
    ws_fr = wb_fr.worksheets[0]

    file_dir_es = get_file_dir(product_template, account.brand_name, str(generation.id), "ES", variation_product)  # color_variation.template
    wb_es = load_workbook(file_dir_es)
    ws_es = wb_es.worksheets[0]

    file_dir_it = get_file_dir(product_template, account.brand_name, str(generation.id), "IT", variation_product)  # color_variation.template
    wb_it = load_workbook(file_dir_it)
    ws_it = wb_it.worksheets[0]

    designs_number = 0
    for design in designs:
        try:
            designs_number = designs_number + 1
            products_list_uk = []
            products_list_de = []
            products_list_fr = []
            products_list_es = []
            products_list_it = []

            if design.sku_code:
                sku_code = design.sku_code
            else:
                designs_with_sku = Design.objects.exclude(sku_code="")
                if designs_with_sku:
                    code = increment_str(designs_with_sku.last().sku_code)
                else:
                    code = 'DBAAA'

                design.sku_code = code
                design.save()

                sku_code = code

            for color_variation in variations:
                if color_variation.product == variation_product:
                    sku_object = SkuCodes.objects.filter(design=design).filter(color_variation=color_variation)
                    if sku_object:
                        sku_code_object = sku_object.first()
                        # sku_code = sku_code_object.code
                        generated_img_url = sku_code_object.img_url

                    else:
                        with open(cwd + "/no_images.txt", "a") as no_images_file:
                            no_images_file.write("Design id: " + str(design.id) + "; Color variation id: " + str(color_variation.id) + "\n")
                            no_images_file.close()

                        generated_img_url = image_generation(color_variation, design.docfile.url, generation)

                        sku_code_object = SkuCodes(color_variation=color_variation, design=design, code=sku_code, img_url=generated_img_url, generation=generation)
                        sku_code_object.save()

                    product = color_variation.product
                    file_name = product.title
                    for market in markets:
                        # file_dir = get_file_dir('clothing', account.brand_name, str(product.id), str(generation.id), market) #color_variation.template
                        # wb = load_workbook(file_dir)
                        # ws = wb.worksheets[0]

                        if market == "UK":
                            products_list_uk = fill_UK_sheet(products_list_uk, ws_uk, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "DE":
                            products_list_de = fill_DE_sheet(products_list_de, ws_de, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "FR":
                            products_list_fr = fill_FR_sheet(products_list_fr, ws_fr, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "ES":
                            products_list_es = fill_ES_sheet(products_list_es, ws_es, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "IT":
                            products_list_es = fill_IT_sheet(products_list_it, ws_it, product, color_variation, host_name, account, sku_code, design, generated_img_url)

            generation.completed_rows = designs_number
            generation.save()
        except Exception as e:
            with open(cwd + "/errors.txt", "a") as output_file:
                output_file.write("2. " + str(e) + ". Design id: " + str(design.id) + "\n")
                output_file.close()
            generation.failed_rows = generation.failed_rows + 1
            generation.save()

            generationError = GenerationError(generation=generation, design=design)
            generationError.save()

    wb_uk.save(file_dir_uk)
    wb_de.save(file_dir_de)
    wb_fr.save(file_dir_fr)
    wb_es.save(file_dir_es)
    wb_it.save(file_dir_it)

    file_path = file_dir_uk
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_uk, file_path=file_path, file_name=file_name, market='UK')
    generated_file.save()

    file_path = file_dir_de
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_de, file_path=file_path, file_name=file_name, market='DE')
    generated_file.save()

    file_path = file_dir_fr
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_fr, file_path=file_path, file_name=file_name, market='FR')
    generated_file.save()

    file_path = file_dir_es
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_es, file_path=file_path, file_name=file_name, market='ES')
    generated_file.save()

    file_path = file_dir_it
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_it, file_path=file_path, file_name=file_name, market='IT')
    generated_file.save()

    end = time.time()
    generation.generated = True
    generation.elapsed_time = end - start
    generation.save()


def fill_excel_mugs(variations, host_name, account, from_date, product_template, variation_product, generation):
    cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    file_name = ""
    start = time.time()
    designs_ids = []
    account_designs = AccountDesign.objects.filter(account=account)
    for account_design in account_designs:
        designs_ids.append(account_design.design.id)

    designs = Design.objects.filter(account=account) | Design.objects.filter(id__in=designs_ids)
    if from_date:
        designs = designs.filter(created_at__gte=from_date)

    markets = ["UK", "DE", "FR", "ES", "IT"]
    # generation = Generation(account=account, total_rows=len(designs))
    generation.total_rows = len(designs)
    generation.save()

    file_dir_uk = get_file_dir(product_template, account.brand_name, str(generation.id), "UK", variation_product)  # color_variation.template
    wb_uk = load_workbook(file_dir_uk)
    ws_uk = wb_uk.worksheets[0]

    file_dir_de = get_file_dir(product_template, account.brand_name, str(generation.id), "DE", variation_product)  # color_variation.template
    wb_de = load_workbook(file_dir_de)
    ws_de = wb_de.worksheets[0]

    file_dir_fr = get_file_dir(product_template, account.brand_name, str(generation.id), "FR", variation_product)  # color_variation.template
    wb_fr = load_workbook(file_dir_fr)
    ws_fr = wb_fr.worksheets[0]

    file_dir_es = get_file_dir(product_template, account.brand_name, str(generation.id), "ES", variation_product)  # color_variation.template
    wb_es = load_workbook(file_dir_es)
    ws_es = wb_es.worksheets[0]

    file_dir_it = get_file_dir(product_template, account.brand_name, str(generation.id), "IT", variation_product)  # color_variation.template
    wb_it = load_workbook(file_dir_it)
    ws_it = wb_it.worksheets[0]

    designs_number = 0
    for design in designs:
        try:
            designs_number = designs_number + 1
            products_list_uk = []
            products_list_de = []
            products_list_fr = []
            products_list_es = []
            products_list_it = []

            if design.sku_code:
                sku_code = design.sku_code
            else:
                designs_with_sku = Design.objects.exclude(sku_code="")
                if designs_with_sku:
                    code = increment_str(designs_with_sku.last().sku_code)
                else:
                    code = 'DBAAA'

                design.sku_code = code
                design.save()

                sku_code = code

            for color_variation in variations:
                if color_variation.product == variation_product:
                    sku_object = SkuCodes.objects.filter(design=design).filter(color_variation=color_variation)
                    if sku_object:
                        sku_code_object = sku_object.first()
                        # sku_code = sku_code_object.code
                        generated_img_url = sku_code_object.img_url

                    else:
                        with open(cwd + "/no_images.txt", "a") as no_images_file:
                            no_images_file.write("Design id: " + str(design.id) + "; Color variation id: " + str(color_variation.id) + "\n")
                            no_images_file.close()

                        generated_img_url = image_generation(color_variation, design.docfile.url, generation)

                        sku_code_object = SkuCodes(color_variation=color_variation, design=design, code=sku_code, img_url=generated_img_url, generation=generation)
                        sku_code_object.save()

                    product = color_variation.product
                    file_name = product.title
                    for market in markets:
                        if market == "UK":
                            products_list_uk = fill_mugs_sheet(products_list_uk, ws_uk, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "DE":
                            products_list_de = fill_mugs_sheet(products_list_de, ws_de, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "FR":
                            products_list_fr = fill_mugs_sheet(products_list_fr, ws_fr, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "ES":
                            products_list_es = fill_mugs_sheet(products_list_es, ws_es, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "IT":
                            products_list_it = fill_mugs_sheet(products_list_it, ws_it, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)

            generation.completed_rows = designs_number
            generation.save()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            msg = "%s %s %s\n" % (exc_type, fname, exc_tb.tb_lineno)

            with open(cwd + "/errors.txt", "a") as output_file:
                output_file.write("3. " + str(e) + ". Design id: " + str(design.id) + "\n")
                output_file.write(msg)
                output_file.close()

            generation.failed_rows = generation.failed_rows + 1
            generation.save()

            generationError = GenerationError(generation=generation, design=design)
            generationError.save()

    wb_uk.save(file_dir_uk)
    wb_de.save(file_dir_de)
    wb_fr.save(file_dir_fr)
    wb_es.save(file_dir_es)
    wb_it.save(file_dir_it)

    file_path = file_dir_uk
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_uk, file_path=file_path, file_name=file_name, market='UK')
    generated_file.save()

    file_path = file_dir_de
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_de, file_path=file_path, file_name=file_name, market='DE')
    generated_file.save()

    file_path = file_dir_fr
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_fr, file_path=file_path, file_name=file_name, market='FR')
    generated_file.save()

    file_path = file_dir_es
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_es, file_path=file_path, file_name=file_name, market='ES')
    generated_file.save()

    file_path = file_dir_it
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_it, file_path=file_path, file_name=file_name, market='IT')
    generated_file.save()

    end = time.time()
    generation.generated = True
    generation.elapsed_time = end - start
    generation.save()


def fill_excel_luggage(variations, host_name, account, from_date, product_template, variation_product, generation):
    cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    file_name = ""
    start = time.time()
    designs_ids = []
    account_designs = AccountDesign.objects.filter(account=account)
    for account_design in account_designs:
        designs_ids.append(account_design.design.id)

    designs = Design.objects.filter(account=account) | Design.objects.filter(id__in=designs_ids)
    if from_date:
        designs = designs.filter(created_at__gte=from_date)

    markets = ["UK", "DE", "FR", "ES", "IT"]
    # generation = Generation(account=account, total_rows=len(designs))
    generation.total_rows = len(designs)
    generation.save()

    file_dir_uk = get_file_dir(product_template, account.brand_name, str(generation.id), "UK", variation_product)  # color_variation.template
    wb_uk = load_workbook(file_dir_uk)
    ws_uk = wb_uk.worksheets[0]

    file_dir_de = get_file_dir(product_template, account.brand_name, str(generation.id), "DE", variation_product)  # color_variation.template
    wb_de = load_workbook(file_dir_de)
    ws_de = wb_de.worksheets[0]

    file_dir_fr = get_file_dir(product_template, account.brand_name, str(generation.id), "FR", variation_product)  # color_variation.template
    wb_fr = load_workbook(file_dir_fr)
    ws_fr = wb_fr.worksheets[0]

    file_dir_es = get_file_dir(product_template, account.brand_name, str(generation.id), "ES", variation_product)  # color_variation.template
    wb_es = load_workbook(file_dir_es)
    ws_es = wb_es.worksheets[0]

    file_dir_it = get_file_dir(product_template, account.brand_name, str(generation.id), "IT", variation_product)  # color_variation.template
    wb_it = load_workbook(file_dir_it)
    ws_it = wb_it.worksheets[0]

    designs_number = 0
    for design in designs:
        try:
            designs_number = designs_number + 1
            products_list_uk = []
            products_list_de = []
            products_list_fr = []
            products_list_es = []
            products_list_it = []

            if design.sku_code:
                sku_code = design.sku_code
            else:
                designs_with_sku = Design.objects.exclude(sku_code="")
                if designs_with_sku:
                    code = increment_str(designs_with_sku.last().sku_code)
                else:
                    code = 'DBAAA'

                design.sku_code = code
                design.save()

                sku_code = code

            for color_variation in variations:
                if color_variation.product == variation_product:
                    sku_object = SkuCodes.objects.filter(design=design).filter(color_variation=color_variation)
                    if sku_object:
                        sku_code_object = sku_object.first()
                        # sku_code = sku_code_object.code
                        generated_img_url = sku_code_object.img_url

                    else:
                        with open(cwd + "/no_images.txt", "a") as no_images_file:
                            no_images_file.write("Design id: " + str(design.id) + "; Color variation id: " + str(color_variation.id) + "\n")
                            no_images_file.close()

                        generated_img_url = image_generation(color_variation, design.docfile.url, generation)

                        sku_code_object = SkuCodes(color_variation=color_variation, design=design, code=sku_code, img_url=generated_img_url, generation=generation)
                        sku_code_object.save()

                    product = color_variation.product
                    file_name = product.title
                    for market in markets:
                        if market == "UK":
                            products_list_uk = fill_luggage_sheet(products_list_uk, ws_uk, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "DE":
                            products_list_de = fill_luggage_sheet(products_list_de, ws_de, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "FR":
                            products_list_fr = fill_luggage_sheet(products_list_fr, ws_fr, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "ES":
                            products_list_es = fill_luggage_sheet(products_list_es, ws_es, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)
                        if market == "IT":
                            products_list_it = fill_luggage_sheet(products_list_it, ws_it, product, color_variation, host_name, account, sku_code, design, generated_img_url, market)

            generation.completed_rows = designs_number
            generation.save()
        except Exception as e:
            with open(cwd + "/errors.txt", "a") as output_file:
                output_file.write("4. " + str(e) + ". Design id: " + str(design.id) + "\n")
                output_file.close()

            generation.failed_rows = generation.failed_rows + 1
            generation.save()

            generationError = GenerationError(generation=generation, design=design)
            generationError.save()

    wb_uk.save(file_dir_uk)
    wb_de.save(file_dir_de)
    wb_fr.save(file_dir_fr)
    wb_es.save(file_dir_es)
    wb_it.save(file_dir_it)

    file_path = file_dir_uk
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_uk, file_path=file_path, file_name=file_name, market='UK')
    generated_file.save()

    file_path = file_dir_de
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_de, file_path=file_path, file_name=file_name, market='DE')
    generated_file.save()

    file_path = file_dir_fr
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_fr, file_path=file_path, file_name=file_name, market='FR')
    generated_file.save()

    file_path = file_dir_es
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_es, file_path=file_path, file_name=file_name, market='ES')
    generated_file.save()

    file_path = file_dir_it
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_it, file_path=file_path, file_name=file_name, market='IT')
    generated_file.save()

    end = time.time()
    generation.generated = True
    generation.elapsed_time = end - start
    generation.save()


def fill_excel_bags(variations, host_name, account, from_date, product_template, variation_product, generation):
    cwd = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    file_name = ""
    start = time.time()
    designs_ids = []
    account_designs = AccountDesign.objects.filter(account=account)
    for account_design in account_designs:
        designs_ids.append(account_design.design.id)

    designs = Design.objects.filter(account=account) | Design.objects.filter(id__in=designs_ids)
    if from_date:
        designs = designs.filter(created_at__gte=from_date)

    markets = ["UK", "DE", "FR", "ES", "IT"]
    # generation = Generation(account=account, total_rows=len(designs))
    generation.total_rows = len(designs)
    generation.save()

    file_dir_uk = get_file_dir(product_template, account.brand_name, str(generation.id), "UK", variation_product)  # color_variation.template
    wb_uk = load_workbook(file_dir_uk)
    ws_uk = wb_uk.worksheets[0]

    # print file_dir_uk

    file_dir_de = get_file_dir(product_template, account.brand_name, str(generation.id), "DE", variation_product)  # color_variation.template
    wb_de = load_workbook(file_dir_de)
    ws_de = wb_de.worksheets[0]

    file_dir_fr = get_file_dir(product_template, account.brand_name, str(generation.id), "FR", variation_product)  # color_variation.template
    wb_fr = load_workbook(file_dir_fr)
    ws_fr = wb_fr.worksheets[0]

    file_dir_es = get_file_dir(product_template, account.brand_name, str(generation.id), "ES", variation_product)  # color_variation.template
    wb_es = load_workbook(file_dir_es)
    ws_es = wb_es.worksheets[0]

    file_dir_it = get_file_dir(product_template, account.brand_name, str(generation.id), "IT", variation_product)  # color_variation.template
    wb_it = load_workbook(file_dir_it)
    ws_it = wb_it.worksheets[0]

    designs_number = 0
    for design in designs:
        try:
            designs_number = designs_number + 1
            products_list_uk = []
            products_list_de = []
            products_list_fr = []
            products_list_es = []
            products_list_it = []

            if design.sku_code:
                sku_code = design.sku_code
            else:
                designs_with_sku = Design.objects.exclude(sku_code="")
                if designs_with_sku:
                    code = increment_str(designs_with_sku.last().sku_code)
                else:
                    code = 'DBAAA'

                design.sku_code = code
                design.save()

                sku_code = code

            for color_variation in variations:
                if color_variation.product == variation_product:
                    sku_object = SkuCodes.objects.filter(design=design).filter(color_variation=color_variation)
                    if sku_object:
                        sku_code_object = sku_object.first()
                        # sku_code = sku_code_object.code
                        generated_img_url = sku_code_object.img_url

                    else:
                        with open(cwd + "/no_images.txt", "a") as no_images_file:
                            no_images_file.write("Design id: " + str(design.id) + "; Color variation id: " + str(color_variation.id) + "\n")
                            no_images_file.close()

                        generated_img_url = image_generation(color_variation, design.docfile.url, generation)

                        sku_code_object = SkuCodes(color_variation=color_variation, design=design, code=sku_code, img_url=generated_img_url, generation=generation)
                        sku_code_object.save()

                    product = color_variation.product
                    file_name = product.title
                    for market in markets:
                        if market == "UK":
                            products_list_uk = fill_UK_bags_sheet(products_list_uk, ws_uk, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "DE":
                            products_list_de = fill_DE_bags_sheet(products_list_de, ws_de, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "FR":
                            products_list_fr = fill_FR_bags_sheet(products_list_fr, ws_fr, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "ES":
                            products_list_es = fill_ES_bags_sheet(products_list_es, ws_es, product, color_variation, host_name, account, sku_code, design, generated_img_url)
                        if market == "IT":
                            products_list_it = fill_IT_bags_sheet(products_list_it, ws_it, product, color_variation, host_name, account, sku_code, design, generated_img_url)

            generation.completed_rows = designs_number
            generation.save()
        except Exception as e:
            with open(cwd + "/errors.txt", "a") as output_file:
                output_file.write("5. " + str(e) + ". Design id: " + str(design.id) + "\n")
                output_file.close()

            generation.failed_rows = generation.failed_rows + 1
            generation.save()

            generationError = GenerationError(generation=generation, design=design)
            generationError.save()

    wb_uk.save(file_dir_uk)
    wb_de.save(file_dir_de)
    wb_fr.save(file_dir_fr)
    wb_es.save(file_dir_es)
    wb_it.save(file_dir_it)

    file_path = file_dir_uk
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_uk, file_path=file_path, file_name=file_name, market='UK')
    generated_file.save()

    file_path = file_dir_de
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_de, file_path=file_path, file_name=file_name, market='DE')
    generated_file.save()

    file_path = file_dir_fr
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_fr, file_path=file_path, file_name=file_name, market='FR')
    generated_file.save()

    file_path = file_dir_es
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_es, file_path=file_path, file_name=file_name, market='ES')
    generated_file.save()

    file_path = file_dir_it
    position = file_path.index('/media/')
    file_path = file_path[position:]

    generated_file = GeneratedFile(generation=generation, full_file_path=file_dir_it, file_path=file_path, file_name=file_name, market='IT')
    generated_file.save()

    end = time.time()
    generation.generated = True
    generation.elapsed_time = end - start
    generation.save()


def image_generation(color_variation, design_url, generation):
    os_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # os_path = ''
    design = Image.open(os_path + '/public' + design_url).convert("RGBA")
    template = Image.open(os_path + '/public' + color_variation.image.url).convert("RGBA")
    # template_width, template_height = template.size
    design_width, design_height = design.size

    x1 = color_variation.upper_x
    x2 = color_variation.lower_x

    y1 = color_variation.upper_y
    y2 = color_variation.lower_y

    max_x = x2 - x1
    max_y = y2 - y1

    width_ratio = max_x / design_width
    height_ratio = max_y / design_height

    if width_ratio < height_ratio:
        x = max_x
        y = width_ratio * design_height
    else:
        y = max_y
        x = height_ratio * design_width

    if x < max_x:
        starting_x = color_variation.upper_x + (max_x - x) / 2
    else:
        starting_x = color_variation.upper_x

    size = int(x), int(y)
    design = design.resize(size, Image.ANTIALIAS)
    template.paste(design, (int(starting_x), color_variation.upper_y), design)
    last_design = GeneratedDesigns.objects.last()
    if last_design:
        name = str(last_design.id + 1)
    else:
        name = "1"
    template.save(os_path + '/public/media/generated-images/' + name + '.png', format="png", compress_level=1)

    generated_designs = GeneratedDesigns(filename=name + '.png', docfile='/generated-images/' + name + '.png', imgurl='/public/media/generated-images/' + name + '.png', generation=generation)
    generated_designs.save()

    design.close()
    template.close()

    return '/media/generated-images/' + name + '.png'


def increment_char(c):
    """
    Increment an uppercase character, returning 'A' if 'Z' is given
    """
    return chr(ord(c) + 1) if c != 'Z' else 'A'


def increment_str(s):
    lpart = s.rstrip('Z')
    num_replacements = len(s) - len(lpart)
    new_s = lpart[:-1] + increment_char(lpart[-1]) if lpart else 'A'
    new_s += 'A' * num_replacements
    return new_s


@login_required
def import_accounts(request):
    oldDB = OldDB()
    accounts = oldDB.get_old_accounts()

    team = TeamUser.objects.filter(user=request.user)

    if team:
        team_obj = team.first().team
    else:
        return HttpResponse("no team")

    for account in accounts:
        new_account = Account(name=account[1], brand_name=account[2], sku_prefix=account[3], team=team_obj)
        new_account.save()

    return HttpResponse("done")


@login_required
def import_account_designs(request):
    count = 0
    oldDB = OldDB()
    accounts = oldDB.get_old_accounts()

    for account in accounts:
        if account[1] == "WISH DARBINIS":
            account_obj = Account.objects.filter(name=account[1]).first()
            designs = oldDB.get_account_designs(account[0])
            for design in designs:
                if count > 20000:
                    new_design = Design(account=account_obj, docfile=design[8], filename=design[3], title=design[3], search_tags_1=design[4], search_tags_2=design[5], search_tags_3=design[6], sku_code=design[2])
                    new_design.save()
                count = count + 1
                # else:
                # account_obj = Account.objects.filter(name = account[1]).first()
                #     designs = oldDB.get_account_designs(account[0])
                #     for design in designs:
                #         if count < 20000:
                #             new_design = Design(account = account_obj, docfile = design[8], filename = design[3], title = design[3], search_tags_1 = design[4], search_tags_2 = design[5], search_tags_3 = design[6], sku_code = design[2])
                #             new_design.save()
                #         count = count + 1

                # new_account = Account(name = account[1], brand_name = account[2], sku_prefix = account[3], team = team_obj)
                # new_account.save()

    return HttpResponse("done")


@login_required
def delete_sku_codes(request):
    first_code = 60896
    # ids_list = [25911, 25920, 25983, 25994, 26033, 26087, 26094, 26160, 26164, 26174, 26218, 26223, 26226, 26281, 26297, 26300, 26325, 26347, 26350, 26369, 26440, 26451, 26515, 26519, 26503, 26490, 26468, 26463]
    # for file_id in ids_list:
    # img_url = "/media/generated-images/" + str(file_id) + ".png"
    sku_code = SkuCodes.objects.filter(id__gte=first_code).delete()
    return HttpResponse("Done")


@login_required
def delete_account_designs(request):
    # account_names = ["GoodGuys1", "GoodGuys1Black", "BadGuys 1", "BadGuys 1 Black", "Nothingtowear", "WISH DARBINIS", "GERIUKAI"]
    # for account_name in account_names:
    # account = Account.objects.filter(name = account_name).first()
    #     designs = Design.objects.filter(account = account).delete()

    return HttpResponse("Done")


def col2num(col):
    num = 0
    for c in col:
        if c in string.ascii_letters:
            num = num * 26 + (ord(c.upper()) - ord('A')) + 1
    return num


@login_required
def delete_batch(request, batch_id):
    generation = get_object_or_404(Generation, pk=batch_id)

    # delete GeneratedDesigns(image) and SkuCodes(Link)
    GeneratedDesigns.objects.filter(generation=generation).delete()
    SkuCodes.objects.filter(generation=generation).delete()

    # delete generation itself (with GeneratedFile on Cascade)
    generation.delete()

    messages.success(request, "Batch has been deleted successfully.")

    return redirect('generated_files')


@login_required
def mass_download(request):
    if request.method == "POST":
        form = MassDownloadForm(request.POST)

        if form.is_valid():
            skus_str = form.cleaned_data['skus']
            zip_dir_path = os.path.join(proj_settings.PROJECT_ROOT, "public/media/zips")

            if os.path.exists(zip_dir_path):
                shutil.rmtree(zip_dir_path)

            os.mkdir(zip_dir_path)

            for sku in skus_str.split(","):
                sku = sku.strip()

                try:
                    design = Design.objects.get(sku_code=sku)
                    ext = os.path.splitext(os.path.basename(design.docfile.name))[1]
                    new_file_name = sku.upper() + ext

                    shutil.copyfile(design.docfile.path, os.path.join(zip_dir_path, new_file_name))
                except:
                    pass

            shutil.make_archive(os.path.join(proj_settings.PROJECT_ROOT, "public/media/download"), 'zip', zip_dir_path)
            output_file_path = os.path.join(proj_settings.PROJECT_ROOT, "public/media/download.zip")

            zip_file = open(output_file_path, 'r')
            response = HttpResponse(zip_file, content_type='application/force-download')
            response['Content-Disposition'] = 'attachment; filename="%s"' % 'download.zip'
            return response

            # return redirect('mass_download')
    else:
        form = MassDownloadForm()

    context = {
        "form": form
    }

    return render(request, "mass_download.html", context)
