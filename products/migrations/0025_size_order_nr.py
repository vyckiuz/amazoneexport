# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-21 21:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0024_generation_generated'),
    ]

    operations = [
        migrations.AddField(
            model_name='size',
            name='order_nr',
            field=models.IntegerField(default=0),
        ),
    ]
