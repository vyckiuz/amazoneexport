# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-12 21:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0010_auto_20170407_2109'),
    ]

    operations = [
        migrations.CreateModel(
            name='GeneratedDesigns',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('docfile', models.FileField(upload_to='generated-images')),
                ('filename', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.AlterField(
            model_name='colorvariation',
            name='image',
            field=models.ImageField(blank=True, upload_to='templates'),
        ),
        migrations.AlterField(
            model_name='product',
            name='template',
            field=models.CharField(choices=[('0', 'Clothing'), ('1', 'Bag'), ('2', 'Pillow'), ('3', 'Mugs'), ('4', 'Hats'), ('5', 'Stickers'), ('6', 'Home')], default='0', max_length=1),
        ),
    ]
