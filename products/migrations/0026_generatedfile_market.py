# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-22 21:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0025_size_order_nr'),
    ]

    operations = [
        migrations.AddField(
            model_name='generatedfile',
            name='market',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
